// This file was mostly taken from the example given here:
// http://www.votchallenge.net/howto/integration.html

// Uncomment line below if you want to use rectangles
#define VOT_RECTANGLE
#include "native/vot.h"
#include <vector>
#include <iostream>

#include "tracker/tracker.h"
#include "network/regressor_train.h"

// selectROI is part of tracking API
#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <fstream>
 
using namespace std;
using namespace cv;

bool leftDown=false, leftUp=false;
Point cor1, cor2;
Rect box;
Mat image, crop;

void mouse_call(int event, int x, int y, int, void*)
{
	if (event == EVENT_LBUTTONDOWN) {
		leftDown = true;
		cor1.x = x;
		cor1.y = y;
		cout << "Corner 1: " << cor1 << endl;
	}
	if (event == EVENT_LBUTTONUP) {
		leftUp = true;
		cor2.x = x;
		cor2.y = y;
		cout << "Corner 2: " << cor2 << endl;
	}
	if (leftDown && not leftUp) {
		Point pt;
		pt.x = x;
		pt.y = y;
		Mat temp_img = image.clone();
		rectangle(temp_img, cor1, pt, Scalar(0,0,255));
		imshow("Video", temp_img);
	}
	if (leftDown && leftUp) {
		box.width  = abs(cor1.x-cor2.x);
		box.height = abs(cor1.y-cor2.y);
		box.x = min(cor1.x, cor2.x);
		box.y = min(cor1.y, cor2.y);
		crop = image(box);
		namedWindow("Cropped Image");
		imshow("Cropped Image", crop);
		leftDown = false;
		leftUp = false;
	}
}

int main (int argc, char *argv[]) {

	/*if (argc < 3) {
	std::cerr << "Usage: " << argv[0]
	      << " deploy.prototxt network.caffemodel"
	      << " [gpu_id]" << std::endl;
	return 1;
	}*/

	if (argc < 2) {
		std::cerr << "Please provide path to image directory." << endl;
		return 1;
	}

	string img_dir = argv[1];
	string img_path = img_dir + "/img%08d.png";

	::google::InitGoogleLogging(argv[0]);

	const string& model_file   = "/home/nathan/dev/Drexel/SeniorDesign/GOTURN/nets/tracker.prototxt";
	const string& trained_file = "/home/nathan/dev/Drexel/SeniorDesign/GOTURN/nets/models/pretrained_model/tracker.caffemodel";

	int gpu_id = 0;
	//if (argc >= 4) {
	//gpu_id = atoi(argv[3]);
	//}

	const bool do_train = false;
	Regressor regressor(model_file, trained_file, gpu_id, do_train);

	// Ensuring randomness for fairness.
	srandom(time(NULL));

	// Create a tracker object.
	const bool show_intermediate_output = false;
	Tracker tracker(show_intermediate_output);

	// open directory of images as video
	VideoCapture cap(img_path); // open the default camera
	if(!cap.isOpened()) return -1;
	
	// create winow
    	namedWindow("Video",1);

	cap >> image;
	imshow("Video", image);
	
	setMouseCallback("Video", mouse_call);
	cout << "\nPress spacebar after selecting target" << endl;
	while(char(waitKey(1)) != ' '){}
	cvDestroyWindow("Cropped Image");
	imshow("Target", crop);

	BoundingBox init_bbox = BoundingBox({cor1.x, cor1.y, cor2.x, cor2.y});
  	tracker.Init(image, init_bbox, &regressor);

    const string& bboxFile = img_dir + "/bboxOut.csv";
    stringstream ss;
    ss << "Frame #, target origin x (pixels), target origin y (pixels), width (pixels), height (pixels)" << endl;

	for(int i;;i++) {
        	cap >> image;
        	if (image.rows > 0 && image.cols > 0) {

			// Track and estimate the bounding box location.
			BoundingBox bbox;
			tracker.Track(image, &regressor, &bbox);

			Point p1 = Point(bbox.x1_, bbox.y1_);
			Point p2 = Point(bbox.x2_, bbox.y2_);

            ss << bbox.x1_ << ",";
            ss << bbox.y1_ << ",";
            ss << bbox.x2_  << ",";
            ss << bbox.y2_ << endl;

			rectangle(image, p1, p2, Scalar(0, 0, 255));
			imshow("Video", image);
			waitKey(1);
		} 
		else {
			break;
		}
		cout << i << endl;
    	}

    ofstream out(bboxFile);
    out << ss.str();
    out.close();

	cvDestroyWindow("Video");
	cvDestroyWindow("Target");


  //##############################################

  // Finishing the communication is completed automatically with the destruction
  // of the communication object (if you are using pointers you have to explicitly
  // delete the object).

  return 0;
}
