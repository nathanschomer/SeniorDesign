// This file was mostly taken from the example given here:
// http://www.votchallenge.net/howto/integration.html

// Uncomment line below if you want to use rectangles
#define VOT_RECTANGLE
#include "native/vot.h"
#include <vector>
#include <iostream>
#include <string>
#include "dirent.h"
#include "sys/stat.h"
#include <fstream>

#include <chrono>
using namespace std::chrono;

#include "tracker/tracker.h"
#include "network/regressor_train.h"

// selectROI is part of tracking API
#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
 
using namespace std;
using namespace cv;

bool leftDown=false, leftUp=false;
Point cor1, cor2;
Rect box;
Mat image, crop;


int dirExists(const char *path)
{
	struct stat info;

	if(stat( path, &info ) != 0)
	        return 0;
	else if(info.st_mode & S_IFDIR)
		return 1;
	else
		return 0;
}

int main (int argc, char *argv[]) {

	if (argc < 2) {
		std::cerr << "Please provide path to data directory." << endl;
		return 1;
	}

	string base_dir = argv[1];
	string img_name = "/%08d.jpg";
	string img_path;

	::google::InitGoogleLogging(argv[0]);

	// TODO: make these paths relative
	const string& model_file   = "/home/nathan/dev/SeniorDesign/GOTURN/nets/tracker.prototxt";
	const string& trained_file = "/home/nathan/dev/SeniorDesign/GOTURN/nets/models/pretrained_model/tracker.caffemodel";

	int gpu_id = 0;

	const bool do_train = false;
	Regressor regressor(model_file, trained_file, gpu_id, do_train);

	// Ensuring randomness for fairness.
	srandom(time(NULL));

	// Create a tracker object.
	const bool show_intermediate_output = false;
	Tracker tracker(show_intermediate_output);

	// TODO: open directory of images as video
	struct dirent *dir;
	vector<string> dirs;
	DIR *d = opendir(base_dir.c_str());

	while((dir = readdir(d)) != NULL)
		if(dir->d_type == DT_DIR){ // this may not work on all systems
			string tmpDir = dir->d_name;
			if(tmpDir.compare("..") != 0 && tmpDir.compare(".") != 0) {
				dirs.push_back(tmpDir);
			}
		}

	double totalTime = 0;
	int    frameCnt  = 0;

	// TODO: for each directory...
	for(int i = 0; i < dirs.size(); i++) {
		const string& img_path = base_dir + "/" + dirs[i] + img_name;
		const string& bbox_groundtruth_path = base_dir + "/" + dirs[i] + "/groundtruth.txt";

		cout << dirs[i] << endl;

		VideoCapture cap(img_path); // open the default camera
		if(!cap.isOpened()) return -1;
		
		cap >> image;
		string truthCSV;

		FILE* bbox_groundtruth_file_ptr = fopen(bbox_groundtruth_path.c_str(), "r");
      		double Ax, Ay, Bx, By, Cx, Cy, Dx, Dy;

		const int status = fscanf(bbox_groundtruth_file_ptr, 
				"%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
                   		&Ax, &Ay, &Bx, &By, &Cx, &Cy, &Dx, &Dy);

		BoundingBox init_bbox = BoundingBox();
		init_bbox.x1_ = std::min(Ax, std::min(Bx, std::min(Cx, Dx))) - 1;
		init_bbox.y1_ = std::min(Ay, std::min(By, std::min(Cy, Dy))) - 1;
		init_bbox.x2_ = std::max(Ax, std::max(Bx, std::max(Cx, Dx))) - 1;
		init_bbox.y2_ = std::max(Ay, std::max(By, std::max(Cy, Dy))) - 1;
		tracker.Init(image, init_bbox, &regressor);

		for(int i;;i++) {
			cap >> image;
			if (image.rows > 0 && image.cols > 0) {

				high_resolution_clock::time_point t1 = high_resolution_clock::now();

				// Track and estimate the bounding box location.
				BoundingBox bbox;
				tracker.Track(image, &regressor, &bbox);
				
				high_resolution_clock::time_point t2 = high_resolution_clock::now();
				totalTime += duration_cast<nanoseconds>(t2 - t1).count();
				frameCnt++;

			} 
			else {
				break;
			}
		}
	}

	double avgFPS = (frameCnt / totalTime)*1000000000;
	cout << endl << ">> FINAL Average FPS: " << avgFPS << endl;
	
  return 0;
}
