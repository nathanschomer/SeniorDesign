// This file was mostly taken from the example given here:
// http://www.votchallenge.net/howto/integration.html

// Uncomment line below if you want to use rectangles
#define VOT_RECTANGLE
#include "native/vot.h"
#include <vector>
#include <iostream>
#include <string>
#include "dirent.h"
#include "sys/stat.h"
#include <fstream>

#include "tracker/tracker.h"
#include "network/regressor_train.h"

// selectROI is part of tracking API
#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#define SHOW_VIDEO 0

using namespace std;
using namespace cv;

bool leftDown=false, leftUp=false;
Point cor1, cor2;
Rect box;
Mat image, crop;


int dirExists(const char *path)
{
	struct stat info;

	if(stat( path, &info ) != 0)
	        return 0;
	else if(info.st_mode & S_IFDIR)
		return 1;
	else
		return 0;
}

int main (int argc, char *argv[]) {

	if (argc < 3) {
		std::cerr << "Usage: $ benchmark_track [input images] [caffe model]." << endl;
		return 1;
	}

	string base_dir = argv[1];
	string img_name = "/img%08d.png";
	string img_path;

	::google::InitGoogleLogging(argv[0]);

	// TODO: make these paths relative
	const string& model_file   = "/home/nathan/dev/Drexel/SeniorDesign/GOTURN/nets/tracker.prototxt";
	//const string& trained_file = "/home/nathan/dev/Drexel/SeniorDesign/GOTURN/nets/models/pretrained_model/tracker.caffemodel";

    string trained_file = argv[2];

	int gpu_id = 0;

	const bool do_train = false;
	Regressor regressor(model_file, trained_file, gpu_id, do_train);

	// Ensuring randomness for fairness.
	srandom(time(NULL));

	// Create a tracker object.
	const bool show_intermediate_output = false;
	Tracker tracker(show_intermediate_output);

	// TODO: open directory of images as video
	struct dirent *dir;
	vector<string> dirs;
	DIR *d = opendir(base_dir.c_str());

	while((dir = readdir(d)) != NULL)
		if(dir->d_type == DT_DIR){ // this may not work on all systems
			string tmpDir = dir->d_name;
			if(tmpDir.compare("..") != 0 && tmpDir.compare(".") != 0) {
				dirs.push_back(tmpDir);
			}
		}

	// TODO: for each directory...
	for(int i = 0; i < dirs.size(); i++) {
		const string& img_path = base_dir + "/" + dirs[i] + img_name;
		const string& bboxFile = base_dir + "/" + dirs[i] + "/" + "bboxOut.csv";
		const string& bbox_groundtruth_path = base_dir + "/" + dirs[i] + "/" + dirs[i] + "_bboxTruth.csv";

		cout << dirs[i] << endl;

		VideoCapture cap(img_path); // open the default camera
		if(!cap.isOpened()) return -1;
		
		cap >> image;

#if SHOW_VIDEO
		namedWindow("Video",1);
		imshow("Video", image);
#endif
		
		// TODO: read first line of groundtruth.txt
		// TODO: 	     ...create Bounding Box
		//ifstream infile(truthFile);
		string truthCSV;

		FILE* bbox_groundtruth_file_ptr;

        cout << "Opening: " << bbox_groundtruth_path.c_str() << endl;
        bbox_groundtruth_file_ptr = fopen(bbox_groundtruth_path.c_str(), "r");

    		double x, y, w, h;

      		//const int status = fscanf(bbox_groundtruth_file_ptr, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
            fscanf(bbox_groundtruth_file_ptr, "%*[^\n]\n", NULL); // skip first line

            char buff[15];
      		const int status = fscanf(bbox_groundtruth_file_ptr, 
				"%s %lf, %lf, %lf,%lf\n", buff, &x, &y, &w, &h);

/*        cout << "buff: " << buff << endl;
        cout << "x = " << x << endl;
        cout << "y = " << y << endl;
        cout << "w = " << w << endl;
        cout << "h = " << h << endl;*/

		// Convert to bounding box format.
		BoundingBox init_bbox = BoundingBox();
		/*init_bbox.x1_ = std::min(Ax, std::min(Bx, std::min(Cx, Dx))) - 1;
		init_bbox.y1_ = std::min(Ay, std::min(By, std::min(Cy, Dy))) - 1;
		init_bbox.x2_ = std::max(Ax, std::max(Bx, std::max(Cx, Dx))) - 1;
		init_bbox.y2_ = std::max(Ay, std::max(By, std::max(Cy, Dy))) - 1;*/

		init_bbox.x1_ = x;
		init_bbox.y1_ = y;
		init_bbox.x2_ = x+w;
		init_bbox.y2_ = y+h;
		
        stringstream ss;
		
		cout << "(" << x  << "," << y << "), (";
		cout << x+w << "," << y+h << ")" << endl;

		tracker.Init(image, init_bbox, &regressor);

        ss << "Frame #, target origin x (pixels), target origin y (pixels), width (pixels), height (pixels)" << endl;
        ss << x << ",";
		ss << y << ",";
		ss << w << ",";
		ss << h << endl;

		// keep track of frame position
		for(int i;;i++) {
			cap >> image;
			if (image.rows > 0 && image.cols > 0) {

				// Track and estimate the bounding box location.
				BoundingBox bbox;
				tracker.Track(image, &regressor, &bbox);

				Point p1 = Point(bbox.x1_, bbox.y1_);
				Point p2 = Point(bbox.x2_, bbox.y2_);

				// TODO: write out to file in directory
				// TODO: calculate % overlap with truth data
				ss << bbox.x1_ << ",";
				ss << bbox.y1_ << ",";
				ss << abs(bbox.x2_-bbox.x1_) << ",";
				ss << abs(bbox.y2_-bbox.y1_) << endl;

				rectangle(image, p1, p2, Scalar(0, 0, 255));
#if SHOW_VIDEO
				imshow("Video", image);
#endif
				waitKey(1);
			} 
			else {
				break;
			}
		}

		// write bounding box locations to file
		ofstream out(bboxFile);
		out << ss.str();
		out.close();
	}

#if SHOW_VIDEO
	cvDestroyWindow("Video");
#endif

  return 0;
}
