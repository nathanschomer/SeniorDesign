
Each directory contains at least two files:

	1. bboxOut.txt - output of boundingbox locations from GOTURN
		(format "x1, y1, x2, y2" specifies pixel locations of bounding box corners)
	
	2. groundtruth_rect.txt - truth data
		(format "x, y, w, h" specifies pixel location of first corner and width/height of box)
		
	**NOTE-  some of the truth data is tab-deliminated and some is CSV
