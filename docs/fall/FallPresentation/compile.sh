#!/bin/bash
echo Compiling document...
pdflatex fallPres
biber fallPres
pdflatex fallPres
pdflatex fallPres
echo "Done, Cleaning up!"
for file in $(ls | grep fallPres.[a-oq-s])
	do
	rm $file
done
rm fallPres.toc
