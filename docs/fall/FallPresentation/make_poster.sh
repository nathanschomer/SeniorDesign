#!/bin/bash

for file in `find $1/ -type f -name "*.avi"`; do
#for file in `find $1/videos/ -type f -`; do
    
    # check to see if a poster already exists
    
    if [ ! -e "${file/.avi}.jpg" ]
    then
        # make a poster
        #echo $file
        avconv -i $file -vframes 1 -an -f image2 -y ${file/.avi/}.jpg
    fi
done

