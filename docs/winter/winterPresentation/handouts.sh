#!/bin/bash
cat winterPres.tex | awk 'NR==1{print "\\documentclass[handout]{beamer}"} NR>1{print $0}' >slides.tex

pdflatex slides
biber slides
pdflatex slides
pdflatex slides
mv slides.pdf handout.pdf

rm slides.*

