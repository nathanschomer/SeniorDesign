#!/bin/bash
echo Compiling document...
pdflatex winterPres
biber winterPres
pdflatex winterPres
pdflatex winterPres
echo "Done, Cleaning up!"
for file in $(ls | grep winterPres.[a-oq-s])
	do
	rm $file
done
rm winterPres.toc
