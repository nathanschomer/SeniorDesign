#!/bin/bash
echo Compiling document...
pdflatex springPres
biber springPres
pdflatex springPres
pdflatex springPres
echo "Done, Cleaning up!"
for file in $(ls | grep springPres.[a-oq-s])
	do
	rm $file
done
rm springPres.toc
