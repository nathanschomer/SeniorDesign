#!/bin/bash

echo 'Moving all imu.csv files to imuDadta/'

outDir='imuData'
#mkdir $outDir

for dir in $(ls .)
do
    oldFile=$dir'/imu.csv'
    if [ -f $oldFile ]; then
        newFile=$outDir'/'$dir'_imu.csv'
        cp $oldFile $newFile
    fi
done
