#!/usr/bin/python
import os
import sys
import csv
import pickle
import argparse as AP

import cv2
import lmdb
from tqdm import tqdm
import caffe

#import Image
import numpy as np
from sklearn.utils import shuffle
caffe_root = '/opt/caffe'

def extractArgs():
	parse=AP.ArgumentParser(description='',epilog='')

	imgArgs=parse.add_argument_group('Image Arguments')
	imgArgs.add_argument('-w',default=227,type=int,help='Width of output image, default is 227px')
	imgArgs.add_argument('-l',default=227,type=int,help='Height of output image, default is 227px')

	orgArgs=parse.add_argument_group('Organizational Arguments')
	orgArgs.add_argument('--dataDir',default='./data/',help='Directory to pull pictures from')
	orgArgs.add_argument('--PreProc',action='store_true',help='Just pre-process the data and make the ImageList.pkl file')

	return parse.parse_args()

def processFolder(filepath, subfolder):
	#Open and read the bbox csv, and parse out information
	with open(os.path.join(filepath,subfolder,subfolder+'_bboxTruth.csv'),'rb') as bboxcsv:
		truthReader=csv.reader(bboxcsv,delimiter=',')
		TR=list(truthReader)
		numRows=len(TR)
		imlist={}
		#for each image with a previous image...
		print subfolder
		print TR[1]
		imlist[TR[1][0]]={'bbox':[float(nmbr) for nmbr in TR[1][1:]] }
		for i in range(2,numRows):
			#Add a dictionary entry that is another dictionary that holds:
			#Name of prvious image(for indexing) and bounding box data(for cropping)
			imlist[TR[i][0]]={'last':TR[i-1][0], 'bbox':[float(nmbr) for nmbr in TR[i][1:]] }
	return imlist
 
def indexAllData(filepath):
	dataFolders=(folder for folder in os.listdir(filepath) if os.path.isdir(os.path.join(filepath,folder)))
	dataIndex={}
	for directory in dataFolders:
		dataIndex[directory]=processFolder(filepath,directory)
	with open('ImageList.pkl', 'wb') as saveFile:
		pickle.dump(dataIndex, saveFile, -1)
		print("List saved!")

def findCropArea(img,box):
	#check that we're not in the top left corner
	x1=max(0,box[0])
	y1=max(0,box[1])
	biggestDim=max(box[2],box[3])*2
	#if we're too far right, shift left
	x2=biggestDim*.75+x1
	if x2>=img.shape[1]:
		x2=img.shape[1]-1
	x1=x2-biggestDim
	#if we're too far down, shift up
	y2=biggestDim*.75+y1
	if y2>=img.shape[0]:
		y2=img.shape[0]-1
	y1=y2-biggestDim
	#Do some rounding
	x1=int(x1)
	y1=int(y1)
	x2=int(x2)
	y2=int(y2)
	return [x1,x2,y1,y2]

def cropPair(im1,box1,im2,box2):
	a=findCropArea(im1,box1)
	refbox=[0,0,0,0]
	box=[0,0,0,0]
	#define our resizing ratio, and create a relative bounding box
	tempimg=(im1[a[2]:a[3],a[0]:a[1]])
	refpatch=cv2.resize(tempimg,(227,227))
	patch=cv2.resize(im2[a[2]:a[3],a[0]:a[1]],(227,227))
	rat=227/(a[3]-a[2])
	refbox[0]=(box1[0]-a[0])*rat
	refbox[1]=(box1[1]-a[2])*rat
	refbox[2]=box1[2]*rat
	refbox[3]=box1[3]*rat
	box[0]=(box2[0]-a[0])*rat
	box[1]=(box2[1]-a[2])*rat
	box[2]=box2[2]*rat
	box[3]=box2[3]*rat
	return refpatch, refbox, patch, box

def AsContLMDB(img,label, refimg,reflabel):
	cur=zip(img,label)
	ref=zip(refimg,reflabel)
	img,label, refimg,reflabel=shuffle(img,label, refimg,reflabel,random_state=0)

	buf=10000000000
	env=lmdb.open('current',map_size=buf)
	max_key = env.stat()["entries"]
	print 'There are currently ___ entries in the lmdb'
	print max_key

	with env.begin(write=True) as txn:
		for i in tqdm(range(len(img))):
			print label[i][0]
			datum=caffe.proto.caffe_pb2.Datum()
			datum.channels=img.shape[1]
			datum.height=img.shape[2]
			datum.width=img.shape[3]
			datum.data=img[i].tobytes()
			datum.label=np.array([ int(num) for num in label[i]])
			str_id='{:80}'.format(i+max_key+1)

			txn.put(str_id.encode('ascii'),datum.SerializeToString())

	env=lmdb.open('reference',map_size=buf)
	with env.begin(write=True) as txn:
		for i in tqdm(range(len(refimg))):
			datum=caffe.proto.caffe_pb2.Datum()
			datum.channels=refimg.shape[1]
			datum.height=refimg.shape[2]
			datum.width=refimg.shape[3]
			datum.data=refimg[i].tobytes()
			datum.label=np.array([ int(num) for num in reflabel[i]])
			str_id='{:80}'.format(i+max_key+1)

			txn.put(str_id.encode('ascii'),datum.SerializeToString())



def dostuff(data, filepath, subdir):
	patchlist=[]
	labels=[]
	reflist=[]
	reflabels=[]
	for img in data[subdir].keys():
		if not 'last' in data[subdir][img].keys():
			continue
		ref=data[subdir][img]['last']
		refimg=cv2.imread(os.path.join(filepath,subdir,ref))
		refbox=data[subdir][ref]['bbox']

		curimg=cv2.imread(os.path.join(filepath,subdir,img))
		curbox=data[subdir][img]['bbox']

		refptch,refbx,ptch,bx=cropPair(refimg,refbox,curimg,curbox)
		patchlist.append(ptch)
		labels.append(bx)
		reflist.append(refptch)
		reflabels.append(refbx)
	patchlist=np.array(patchlist)
	labels=np.array(labels)
	reflist=np.array(reflist)
	reflabels=np.array(reflabels)

	AsContLMDB(patchlist,labels,reflist,reflabels)


if __name__ == '__main__':
	#Read command line arguments
	args=extractArgs()

	#Check for indexed training data
	if not os.path.exists('ImageList.pkl'):
		#create index of data if it does not exist
		indexAllData(args.dataDir)

	#Open the file that has pre-processed the data
	if os.path.exists('ImageList.pkl'):
		with open('ImageList.pkl','rb') as saveFile:
			data=pickle.load(saveFile)
	else:
		print("I can't find the save file.")

	if args.PreProc:
		exit

	#open and crop images
	#and save to lmdb
	for subdir in data:
		dostuff(data, args.dataDir, subdir)


