Brian Hosler
Caffe data compilation and training script

To run
	1 cd into the the desired model's directory
	2 ../dataCompilation.py --dataDir /path/to/data/directory/
	3 make sure there is an empty models folder
	4 ../training.py

