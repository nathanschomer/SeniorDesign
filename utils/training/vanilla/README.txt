Vanilla:
This test is to check GOTURN's performance on just videos.
The original model was trained on videos and transformed images,
but we cannot create IMU data to associate with transformed images,
so we want to train the original goturn without images for an accurte
comparison.

Architecture:
	GOTURN
	5 conv layers
	3 fc layers

Data:
	Training:
		ALOV300++
	Testing:
		ALOV300++



