
import os
import lmdb
import caffe
import pickle
import numpy as np
from tqdm import tqdm
from sklearn.utils import shuffle

def AsLMDB(patches,labels,directory):
	patches,labels=shuffle(patches,labels,random_state=0)

	buf=patches.nbytes*10
	env=lmdb.open(directory,map_size=buf)

	with env.begin(write=True) as txn:
		for i in tqdm(range(len(patches))):
			datum=caffe.proto.caffe_pb2.Datum()
			datum.channels=patches.shape[1]
			datum.height=patches.shape[2]
			datum.width=patches.shape[3]
			datum.data=patches[i].tobytes()
			datum.label=int(labels[i])
			str_id='{:80}'.format(i)

			txn.put(str_id.encode('ascii'),datum.SerializeToString())

def AsContLMDB(patches,labels,directory):
	patches,labels=shuffle(patches,labels,random_state=0)

	buf=patches.nbytes*50
	env=lmdb.open(directory,map_size=buf)
	max_key = env.stat()["entries"]
	print 'There are currently ___ entries in the lmdb'
	print max_key

	with env.begin(write=True) as txn:
		for i in tqdm(range(len(patches))):
			datum=caffe.proto.caffe_pb2.Datum()
			datum.channels=patches.shape[1]
			datum.height=patches.shape[2]
			datum.width=patches.shape[3]
			datum.data=patches[i].tobytes()
			str_id='{:80}'.format(i+max_key+1)

			txn.put(str_id.encode('ascii'),datum.SerializeToString())

	buf=patches.nbytes*10
	env=lmdb.open(directory+'Lables',map_size=buf)
	with env.begin(write=True) as txn:
		for i in tqdm(range(len(labels))):
			datum=caffe.proto.caffe_pb2.Datum()
			#datum.data=labels[i].tobytes()
			datum.channels=1
			datum.height=1
			datum.width=1
			datum.float_data.extend(np.array(labels[i]).flat)
			#datum=caffe.io.array_to_datum(np.array(labels[i]).astype(float).reshape(1,1,1))
			str_id='{:80}'.format(i+max_key+1)

			txn.put(str_id.encode('ascii'),datum.SerializeToString())




def pklWrite(args):
	#get a list of all images ending in .JPG, shuffle it, and define the train/test ratio
	
	picList=[f for f in os.listdir(args.DRESDEN) if f[-4:]==".JPG"]
	picList=shuffle(picList)
	cutoff=len(picList)//9

	filepath='save/ImageList.pkl'
	#create a dictionary containing the lists of training and testing images
	data={'train':picList[cutoff:],'test':picList[:cutoff]}
	with open(filepath, 'wb') as saveFile:
		pickle.dump(data, saveFile, -1)
		print("Training and Testing lists saved!")

def pklRead():
	filepath='save/ImageList.pkl'
	if os.path.exists(filepath):
		with open(filepath,'rb') as saveFile:
			data=pickle.load(saveFile)
		return data
	else:
		print("I can't find that save file.\nMake sure it's in the save folder, and that you spelled its name correctly.")
