#pragma once

//#include "PCWFD.h"
#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <string>

#define	MINIMUM_TARGET_SIZE	16

class FernTarget {

public:
	FernTarget();

	bool targetSelected;
	bool targetValidated;
	cv::Mat targetSearchFrame;

	cv::Point targetOrigin;
	//cv::Point targetUpperLeft, targetLowerRight;
	float targetRadius;

	bool drag;

	cv::Rect userSelection;
	cv::Mat roiImg;

	cv::Rect userSelection_original;
	cv::Mat roiImg_original;

	std::string selectionWindowTitle;
	std::string verificationWindowTitle;

	void updateTargetSelection(cv::Point origin, cv::Point mousePoint);

	void setTargetSize(int w, int h, float mag);
	void setTarget(cv::Mat image, cv::Rect roi);

	void printTargetParameters();

	int width(void) {
		return roiImg.cols;
	}

	int height(void) {
		return roiImg.rows;
	}

	int magnitude(void) {
        // return diag of userSelection_original;
        return sqrt(pow(userSelection_original.width, 2)+pow(userSelection_original.height, 2));
	}

	void resetTargetSelection(void) {
		targetSelected = false;
		targetValidated = false;
		drag = false;

		targetSearchFrame = cv::Mat();

		targetOrigin = cv::Point();
		//targetUpperLeft = cv::Point();
		//targetLowerRight = cv::Point();

		userSelection = cv::Rect();
		userSelection_original = cv::Rect();

		roiImg = cv::Mat();
		roiImg_original = cv::Mat();
	}

	void onMouse(int event, int x, int y, int, void*);
	void onMouse2(int event, int x, int y, int, void*);

	void updateTargetView(cv::Mat view, bool &advanceToNext, bool &continueTargetSelect);

	void validateTargetView();
};
