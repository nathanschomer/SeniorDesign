//#include "PCWFD.h"

#include "FernTarget.h"

#include <signal.h>
//#include <chrono>

using namespace cv;
using namespace std;

#define CVT_MONO 0

#define REVERSE	 0

#define RUNTIME	 0 	//number of frames to process

#define DEFAULT_COLS 0
#define DEFAULT_ROWS 0

bool g_QUIT = false;

FernTarget target;

/********************************************************************************************/

cv::Mat nextImage(std::vector<std::string> * files, std::vector<std::string>::iterator *it, int * iteratorOffset, int w, int h, bool mono) {
	//increment iterator
	*it = files->begin() + *iteratorOffset;
	(*iteratorOffset)++;

	//define filename
	std::string fileName = (std::string)**it;
	std::cout << "Using frame " << fileName << std::endl;

	//allocate pixel buffer
	/*size_t szPixBuf = w*h*sizeof(char);
	char * pixBuf = (char*)malloc(szPixBuf);
	memset(pixBuf, 0, szPixBuf);*/

	// Read file into pixel buffer, break loop if error
    // TODO: replace readPixelBuffer
    cv::Mat rawColor = cv::imread(fileName);

    cv::Mat raw;
    cv::cvtColor(rawColor, raw, CV_RGB2GRAY);

	/*if (!pcwfd::readPixelBuffer(fileName, pixBuf, szPixBuf)) {
		return cv::Mat();
	}*/

	// define mat file using pixel buffer
	// cv::Mat raw = cv::Mat(h, w, CV_8UC1, pixBuf);

	if (mono) {
		cv::Mat rawMono;
		cv::cvtColor(raw,rawMono,CV_BayerBG2GRAY);
		return rawMono.clone();
	} else {
		return raw.clone();
	}
}

static void interrupt_onMouse(int event, int x, int y, int i, void* ptr) {
	target.onMouse(event, x, y, i, ptr);
}

static void interrupt_onMouse2(int event, int x, int y, int i, void* ptr) {
	target.onMouse2(event, x, y, i, ptr);
}

void myterminate_handler(int s) {
	printf("Caught signal %d\n", s);

	g_QUIT = true;
}

void sortVec ( std::vector<std::string> *files ) {
    
    // insertion sort
    int i = 1;
    std::string::size_type sz;

    while ( i < files->size() ) {
        
        int j = i;

        while (j > 0 && stoi(files->at(j-1).substr(3,8), &sz) > stoi(files->at(j).substr(3,8), &sz)) {
            std::string tmp;

            // swap!
            tmp = files->at(j-1);
            files->at(j-1) = files->at(j);
            files->at(j) = tmp;
            j--;
        }
        i++;
    }
}

bool getFilesWithExtension(std::string path, std::string ext, std::vector<std::string> *files)
{
    DIR *dir;
    struct dirent *ent;
    int i = 0;

    if ((dir = opendir (path.c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            std::string p = ent->d_name;
            if (p.size() > ext.size()) {
                if( p.substr( (p.size() - ext.size()), ext.size()) == ext) {
                    files->push_back(p);
                }
            }
        }
        closedir (dir);
        
        // sort file list
        sortVec(files);

        // add full path
        for ( int i = 0; i < files->size(); i++)
            files->at(i) = path + files->at(i);

    } else {
        std::cerr << "ERROR -- could not get list of files in " << path << endl;
        return false;
    }
    
    return true;
}

bool targetSelectionProcess(std::vector<std::string> * files, std::vector<std::string>::iterator * it, int * iteratorOffset){
	std::cout << "Beginning Target Selection " << std::endl;

    // TODO: change selection to allow square bbox

	target.targetValidated = false;

	while (!target.targetValidated) {
		//Reset Target Selection Parameters
		target.resetTargetSelection();

		cv::destroyAllWindows();

		cv::Mat readFrame = nextImage(files, it, iteratorOffset, DEFAULT_COLS, DEFAULT_ROWS, CVT_MONO);

        target.targetSearchFrame = readFrame.clone();

		namedWindow(target.selectionWindowTitle, CV_WINDOW_NORMAL);
		setMouseCallback(target.selectionWindowTitle, interrupt_onMouse, 0);

		bool continueTargetSelect = true;
		bool advanceToNext = true;

		while (!target.targetSelected) {

			/* Check Image */
			if (readFrame.empty()) {
				break;
			}
			else {
				//Set the current frame
				target.targetSearchFrame = readFrame.clone();
				Mat viewFrame = target.targetSearchFrame.clone();

				//Update view of the target selection process
				target.updateTargetView(viewFrame, advanceToNext, continueTargetSelect);
				if (!continueTargetSelect) {
					g_QUIT = true;
					return false;
				}
			}

			// If target has not yet been selected, check if user wants to change the frame
			if (!target.targetSelected) {
				if (advanceToNext){
					//Read Next Frame
					readFrame = nextImage(files, it, iteratorOffset, DEFAULT_COLS, DEFAULT_ROWS, CVT_MONO);
				}
				else{
					// Rewind and go back to previous frame
					iteratorOffset--;
					iteratorOffset--;
					readFrame = nextImage(files, it, iteratorOffset, DEFAULT_COLS, DEFAULT_ROWS, CVT_MONO);
				}
			}
		}//End While-Loop

		/********************************************************************************************/
		/** Verify Valid Target **/
		/********************************************************************************************/
		std::cout << "Verifying Target Selection " << std::endl;

		target.printTargetParameters();
		cv::destroyAllWindows();
		namedWindow(target.verificationWindowTitle, CV_WINDOW_NORMAL);
		setMouseCallback(target.verificationWindowTitle, interrupt_onMouse2, 0);
		target.validateTargetView();
	}

	return true;
}

cv::Point2f matchCentroid(cv::Point matchLoc, float scaleFactor) {

	return cv::Point2f( (matchLoc.x + target.roiImg.cols / 2) / scaleFactor, (matchLoc.y + target.roiImg.rows / 2) / scaleFactor );
}


/********************************************************************************************/
/********************************************************************************************/
int main(int argc, const char** argv)
{

	srand(50);

	/********************************************************************************************/
	/* Parse Command Line Options (if any) */
	/********************************************************************************************/
	std::string path, outputFile, folder;

	if (argc == 2) {
		path = stringstream(argv[1]).str();
        folder = path.substr(path.find_last_of("/")+1);

        if (path[path.length() - 1] != '/')
            path += "/";

		outputFile = path + folder + "_bboxTruth.csv";
        std::cout << "folder: " << folder << std::endl;
	}
	else {
		fputs("Error, incorrect number of arguments dummy!\n", stderr);
		return EXIT_FAILURE;
	}

	/********************************************************************************************/
	/*  Setup file variables */
	/********************************************************************************************/
	std::ofstream logFile;
	std::vector<std::string> fileList;
	std::vector<std::string>::iterator iterator;
	int iteratorOffset;

	/********************************************************************************************/
	/*  Get list of all image files */
	/********************************************************************************************/

	// populate file list from given directory and file extension combination
    // TODO: replace this
    
	if (!getFilesWithExtension(path, ".png", &fileList)) {
		fputs("Error finding raw files in directory \n", stderr);
		return EXIT_FAILURE;
	}

#if REVERSE
	// reverse order for easier target pickin'
	std::reverse(fileList.begin(), fileList.end());
#endif

	// set iterators to the first file
	iterator = fileList.begin();
	iteratorOffset = 0;

	/********************************************************************************************/
	/*  Open and initialize log file */
	/********************************************************************************************/

	// Open the logFile using the given path
	std::cout << "Opening log file : " << outputFile << std::endl;
	logFile.open(outputFile.c_str());
	if (!logFile.is_open()) {
		fputs("Error opening specified logFile \n", stderr);
		return EXIT_FAILURE;
	}

// Write logFile header
	logFile << "Frame #, target origin x (pixels), target origin y (pixels), width (pixels), height (pixels)\n";

	/********************************************************************************************/
	/** Set up log file variables **/
	/********************************************************************************************/
	// This data must be calculated each frame for the log file
	cv::Point2f matchCenter(0,0);
	//float wInTheLastFrame = 0;
	//float hInTheLastFrame = 0;
	cv::Rect targetBoundingBox(0,0,0,0);
	cv::Rect prevBbox(0,0,0,0);
	/********************************************************************************************/
	/*  Setup Detector */
	/********************************************************************************************/
	// Set a 2x3 or 3x3 warp matrix depending on the motion model.
	Mat warp_original_to_previous;
	// Initialize the matrix to identity
	warp_original_to_previous = Mat::eye(3, 3, CV_32F);
	/********************************************************************************************/
	/** Set up transformation variables **/
	/********************************************************************************************/
	// roiImg_original is the original template complete with bounding box
	// roiImg is the scaled and rotated template

	float matchingThreshold = 0.80;
	float diagInTheLastFrame = 50.0;
	float diagInTheTemplate = 50.0;
	float desiredDiag = 50.0;
	float imageScaleFactor = 1.0;
	float templateScaleFactor = 1.0;
	Mat warpMatrix = Mat::eye(3, 3, CV_32F);            // warps image to appropriate scale
	Mat warpInitialToPredicted = Mat::eye(3, 3, CV_32F); // for warping template prior to original search
	Mat warpPredictedToMeasuredAffine = Mat::eye(2, 3, CV_32F); // for correcting template warp
	Mat warpPredictedToMeasured = Mat::eye(3, 3, CV_32F); // for correcting template warp
	/********************************************************************************************/
	/** Set parameters for ECC **/
	/********************************************************************************************/
	//// Define the motion model
	const int warp_mode = MOTION_AFFINE;
	// Specify the number of iterations.
	int number_of_iterations = 100;
	// Specify the threshold of the increment
	// in the correlation coefficient between two iterations
	double termination_eps = 1e-10;
	// Define termination criteria
	TermCriteria criteria(TermCriteria::COUNT + TermCriteria::EPS, number_of_iterations, termination_eps);
	/********************************************************************************************/
	/** Select the Target **/
	/********************************************************************************************/
	if (!targetSelectionProcess(&fileList, &iterator, &iteratorOffset)) return EXIT_FAILURE;
	matchCenter  = target.targetOrigin;
	targetBoundingBox = target.userSelection;
	std::cout << "Target manually assigned to " << matchCenter << " with bounding box " << targetBoundingBox << std::endl;

	// Backup the iterator once so that the last frame of the target selection process gets tracked
	iteratorOffset--;

	// Setup initial transformations
	diagInTheTemplate = target.magnitude();
	diagInTheLastFrame = diagInTheTemplate;
	templateScaleFactor = fmin(desiredDiag / diagInTheTemplate, 1.0);
	warpInitialToPredicted.at<float>(0, 0) = templateScaleFactor;
	warpInitialToPredicted.at<float>(1, 1) = templateScaleFactor;

	/********************************************************************************************/
	/** Begin Survey **/
	/********************************************************************************************/

	//Reset
	destroyAllWindows();

	int loopCount = 0;
	bool continueSurvey = true;
	bool confidentInTargetLocation = true;

	while (continueSurvey) { 	/* Start While-loop */
		std::cout << "**********************************************************************" << std::endl;

		// Try and get next frame
		cv::Mat readFrame = nextImage(&fileList, &iterator, &iteratorOffset, DEFAULT_COLS, DEFAULT_ROWS, CVT_MONO);

		/* Check Image */
		if (!readFrame.empty()) {

			std::cout << "File " << iteratorOffset << " of " << fileList.size() << std::endl;
			loopCount++;

			// Matrices for viewing and template matching
			cv::Mat rawFrame = readFrame.clone();
			cv::Mat viewFrame;
			cv::Mat warpedFrame;
			cv::Mat result;

			/********************************************************************/
			// Compute how to downscale image
			/********************************************************************/
            std::cout << "desiredDiag = " << desiredDiag << std::endl;

			imageScaleFactor = fmin(desiredDiag / diagInTheLastFrame, 1.0);
			warpMatrix.at<float>(0, 0) = imageScaleFactor;
			warpMatrix.at<float>(1, 1) = imageScaleFactor;
			cv::Size newFrameSize(rawFrame.cols*imageScaleFactor, rawFrame.rows*imageScaleFactor);
			warpPerspective(rawFrame, warpedFrame, warpMatrix, newFrameSize, INTER_LINEAR);


            /********************************************************************/
			// Compute the predicted template
			/********************************************************************/
			cv::Size newTemplateSize(target.roiImg_original.cols*templateScaleFactor, target.roiImg_original.rows*templateScaleFactor);
			warpPerspective(target.roiImg_original, target.roiImg, warpInitialToPredicted, newTemplateSize, INTER_LINEAR);
			cv::Rect templateCrop(0, 0, newTemplateSize.width, newTemplateSize.height);
			target.roiImg = target.roiImg(templateCrop);
			
            /********************************************************************/
			// Do the template matching
			/********************************************************************/
			/// Do the Matching and Normalize

			cv::matchTemplate(warpedFrame, target.roiImg, result, CV_TM_CCOEFF_NORMED);
			
            /// Localizing the best match with minMaxLoc
			double minVal; double maxVal; Point minLoc; Point maxLoc;
			minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
			matchCenter  = matchCentroid(maxLoc, imageScaleFactor);
			// Define the bounding box
            prevBbox = targetBoundingBox;
			targetBoundingBox = cv::Rect(maxLoc.x, maxLoc.y, target.roiImg.cols, target.roiImg.rows);
			// Print results
			std::cout << "Automated target measurement : " << std::endl;
			//std::cout << "maxLoc = " << maxLoc << std::endl;
			std::cout << "matchCenter = " << matchCenter << std::endl;
			std::cout << "targetBoundingBox = " << targetBoundingBox << std::endl;
			std::cout << "maxVal = " << maxVal << std::endl;
			/*******************************************************************************************/
			//  Are we confident this is the target?
			/*******************************************************************************************/
			confidentInTargetLocation = (maxVal > matchingThreshold);
			/*******************************************************************************************/
			//  Show ATL results
			/*******************************************************************************************/

            viewFrame = warpedFrame.clone();

            if(!confidentInTargetLocation) 
            {
                std::cout << "WARNING -- low confidence." << std::endl;
//                targetBoundingBox = prevBbox;
            }
			    
            cv::rectangle(viewFrame, targetBoundingBox, cv::Scalar(0, 0, 255), 2, cv::LINE_4);

            //cv::imshow("RAW FRAME", rawFrame);
            cv::imshow("TEMPLATE", target.roiImg);
            //cv::imshow("WARPED FRAME", warpedFrame);
            cv::imshow("VIEW FRAME", viewFrame);

			bool halt = false;
			bool skip = false;

            // only pause if we're not confident in target location
			char keyPress = cv::waitKey(confidentInTargetLocation);
            
            //char keyPress = cv::waitKey(0);
			switch (keyPress) {
			case 'h' :
				confidentInTargetLocation = false;
				halt = true;
				break;
			case 'a' :
				matchingThreshold = maxVal;
				confidentInTargetLocation = true;
				break;
			case 's' :
				confidentInTargetLocation = false;
				break;
			case 'q' :
				g_QUIT = true;
				break;
			default :
				std::cout << "keypress = " << keyPress << std::endl;
				confidentInTargetLocation = true;
				break;
			}

			/*******************************************************************************************/
			//  Show ATL results
			/*******************************************************************************************/

			if (confidentInTargetLocation){
				Mat currentMatch = warpedFrame(targetBoundingBox);

				// Run the ECC algorithm. The results are stored in warp_matrix.
				try{
					cv::findTransformECC(
							target.roiImg,
							currentMatch,
							warpPredictedToMeasuredAffine,
							warp_mode,
							criteria
					);
				}
				catch (cv::Exception& e)
				{
					warpPredictedToMeasuredAffine = Mat::eye(2, 3, CV_32F);
				}

				Mat B = cv::Mat(1, 3, warpPredictedToMeasuredAffine.type());
				B.at<float>(0, 0) = 0.0;
				B.at<float>(0, 1) = 0.0;
				B.at<float>(0, 2) = 1.0;
				vconcat(warpPredictedToMeasuredAffine, B, warpPredictedToMeasured);

				warpInitialToPredicted = warpPredictedToMeasured*warpInitialToPredicted;
				//std::cout << "Total Affine Transformation : " << std::endl << warpInitialToPredicted << std::endl;

				Mat rotationScaled = warpInitialToPredicted(Rect(0, 0, 2, 2));
				//std::cout << "rotationScaled : " << std::endl << rotationScaled << std::endl;

				templateScaleFactor = sqrt(cv::determinant(rotationScaled));
				//std::cout << "templateScaleFactor = " << templateScaleFactor << std::endl;

				diagInTheLastFrame = templateScaleFactor*diagInTheTemplate / imageScaleFactor;
				//std::cout << "diagInTheLastFrame = " << diagInTheLastFrame << std::endl;

				//wInTheLastFrame = templateScaleFactor*target.userSelection_original.width / imageScaleFactor;
				//hInTheLastFrame = templateScaleFactor*target.userSelection_original.height / imageScaleFactor;

				// Recenter things
				Mat centerOffset = Mat(2, 1, CV_32F);
				Mat centerOffsetFixed = Mat(2, 1, CV_32F);
				centerOffset.at<float>(0, 0) = (target.roiImg_original.cols) / 2;
				centerOffset.at<float>(1, 0) = (target.roiImg_original.rows) / 2;
				centerOffsetFixed = centerOffset*templateScaleFactor - rotationScaled*centerOffset;
				warpInitialToPredicted.at<float>(0, 2) = centerOffsetFixed.at<float>(0, 0);
				warpInitialToPredicted.at<float>(1, 2) = centerOffsetFixed.at<float>(1, 0);

			} else if (halt) {
				std::cout << "HALT" << std::endl;
				// Back up, then re-select target
       
                
				iteratorOffset--;
				if (!targetSelectionProcess(&fileList, &iterator, &iteratorOffset)) {
					g_QUIT = true;
				} else {
					// Reset all transformation variables
					warpMatrix = Mat::eye(3, 3, CV_32F);
					warpPredictedToMeasuredAffine = Mat::eye(2, 3, CV_32F);
					warpPredictedToMeasured = Mat::eye(3, 3, CV_32F);
					warp_original_to_previous = Mat::eye(3, 3, CV_32F);

					// Re-calculate new transformation parameters
					diagInTheTemplate = target.magnitude();
					diagInTheLastFrame = diagInTheTemplate;
					desiredDiag = 50.0;
					imageScaleFactor = 1.0;
					templateScaleFactor = fmin(desiredDiag / diagInTheTemplate, 1.0);

					// Re-calculate new initial transformation
					warpInitialToPredicted = Mat::eye(3, 3, CV_32F);
					warpInitialToPredicted.at<float>(0, 0) = templateScaleFactor;
					warpInitialToPredicted.at<float>(1, 1) = templateScaleFactor;

					//Manually assign the target based on user selection
					matchCenter  = target.targetOrigin;
					targetBoundingBox = target.userSelection;
					std::cout << "Target manually assigned to " << matchCenter << " with bounding box " << targetBoundingBox << std::endl;

					cv::waitKey();
				}
				std::cout << "RESUME" << std::endl;
			}

			// Write results to log file
			if (!skip && !g_QUIT) {
				// logfile data:
				std::string fullName = (std::string)*iterator;
				long pos = fullName.find_last_of('/');
				std::string shortName = fullName.substr(pos + 1);

                // calculate location of top left corner
                //   top left = center - ( DIM / 2 )
                float scaleFactor = sqrt(target.targetSearchFrame.rows^2 + target.targetSearchFrame.cols^2) / diagInTheLastFrame;
                float width = targetBoundingBox.width/imageScaleFactor;
                float height = targetBoundingBox.height/imageScaleFactor;

                cv::Point2f center_to_origin_offset(width/2, height/2);
                cv::Point2f topLeft = matchCenter;
                
                if(!halt) { 
                    topLeft -= center_to_origin_offset;
                }
                
				// save to log file
				std::ostringstream ss;
//				ss << shortName << ", " << topLeft.x << ", " << topLeft.y << ", " << viewFrame.rows << "," << viewFrame.cols << std::endl;
                
				ss << shortName << ", " << topLeft.x << ", " << topLeft.y << ", " << width << "," << height << std::endl;

                logFile << ss.str();
				std::cout << ss.str();
			}

			// Check for fixed end conditions
			if ((g_QUIT) || (RUNTIME > 0 && loopCount >= RUNTIME) || (iteratorOffset >= fileList.size()) ) {
				continueSurvey = false;
			}

		} else {
			// There was an error grabbing the readFrame
			std::fputs("Error grabbing readFrame", stderr);
			break;
		}
	}/* End While-loop */


	/* Close logFile */
	logFile.close();

	std::cout << "Finished " << std::endl;

	/* Exit */
	return 0;
}
