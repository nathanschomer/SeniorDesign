#include "FernTarget.h"

FernTarget::FernTarget() {
	resetTargetSelection();
	selectionWindowTitle = "Target Selection Frame | Draw From Target Center";
	verificationWindowTitle = "Validate Target | Click or Y to confirm, or 'q' to quit";
}

void FernTarget::printTargetParameters() {
	//std::cout << "Target Selected at " << userSelection.x << "," << userSelection.y << std::endl;
	std::cout << "Target Origin : " << targetOrigin << std::endl;
	// std::cout << "Target Magnitude : " << 2*targetRadius << std::endl;
	std::cout << "Target Size " << roiImg.cols << "x" << roiImg.rows << std::endl;

}

void FernTarget::setTarget(cv::Mat image, cv::Rect roi) {

	float targetMagnitude = sqrt(roi.width*roi.width + roi.height*roi.height);

	/* Check to see if userSelection is valid */
	if (roi.width > 0 && roi.height > 0 && targetMagnitude >= MINIMUM_TARGET_SIZE) {
		/* Grab associated roi */
		roiImg = targetSearchFrame(roi);
		/* Check to see if roi is valid */
		if (!roiImg.empty()) {
			//setTargetSize(roiImg.cols, roiImg.rows, targetSize);
			userSelection_original = roi;
			roiImg_original = roiImg.clone(); // Added because this will become warped and stuff
			targetSelected = true;
		}
	}
}

void FernTarget::updateTargetSelection(cv::Point origin, cv::Point mousePoint) {
    // TODO: change this so it displays a rectangle
	//targetRadius = sqrt(pow(origin.x - mousePoint.x, 2) + pow(origin.y - mousePoint.y,2));
	//userSelection = cv::Rect(origin.x - targetRadius, origin.y - targetRadius, targetRadius*2, targetRadius*2);

    userSelection = cv::Rect(origin, mousePoint);
}

void FernTarget::onMouse(int event, int x, int y, int, void*)
{
    // TODO: change call to updateTargetSelection
	if (event == CV_EVENT_LBUTTONDOWN && !drag)
	{
		/* left button clicked. ROI selection begins */
		targetOrigin = cv::Point(x, y);
		drag = true;
	}

	if (event == CV_EVENT_MOUSEMOVE && drag)
	{
		/* left button down. ROI selection in progress */
		updateTargetSelection(targetOrigin, cv::Point(x, y));

	}

	if (event == CV_EVENT_LBUTTONUP && drag)
	{
		/* left button released. ROI selection completed */
		drag = false;

		updateTargetSelection(targetOrigin, cv::Point(x, y));
		setTarget(targetSearchFrame, userSelection);
	}
}

void FernTarget::onMouse2(int event, int x, int y, int, void*)
{
	if (event == CV_EVENT_LBUTTONDOWN)
	{
		targetValidated = true;
	}
}

void FernTarget::updateTargetView(cv::Mat viewFrame, bool &advanceToNext, bool &continueTargetSelect) {

	/* Show Target Image to Enable Target Selection */
	advanceToNext = true;
	if (!viewFrame.empty()) {
		bool updateView = true;
		std::cout << "target identification window " << std::endl;
		std::cout << "click on the target or:" << std::endl;
		std::cout << "q,Q,esc: quit" << std::endl;
		while (updateView && !targetSelected) {
			//Draw current user selection
			cv::Mat tmpFrame;
			/*if (tmpFrame.channels() == 1) {
				//cv::cvtColor(viewFrame, tmpFrame, CV_GRAY2BGR);
			} else {*/
				tmpFrame = viewFrame.clone();
		//	}

            // TODO NATHAN: change drawing here?
			cv::rectangle(tmpFrame, userSelection, cv::Scalar(0, 0, 255), 2, cv::LINE_4);

			//cv::circle(tmpFrame, targetOrigin, 4, cv::Scalar(0, 255, 0), -1, cv::LINE_AA);
			//cv::circle(tmpFrame, targetOrigin, targetRadius, cv::Scalar(0,255,0), 1, cv::LINE_AA);

			std::ostringstream selectionSizeTxt; selectionSizeTxt << userSelection.width << " x " << userSelection.height;
			cv::putText(tmpFrame, selectionSizeTxt.str().c_str(), cv::Point(userSelection.x, userSelection.y), cv::FONT_HERSHEY_SIMPLEX, .5, cv::Scalar(0, 0, 255), 1, cv::LINE_AA);

			/* Show Image */
			cv::imshow(selectionWindowTitle, tmpFrame);
			/* Check for key */
			switch ((char)cv::waitKey(30)) {
			case 'q':
			case 'Q':
				advanceToNext = false;
				updateView = false;
				continueTargetSelect = false;
				break;
			case 'S':
				std::cout << "got ->" << std::endl;
				//do nothing
				updateView = false;
				break;
			case 27: //escape key
				//OnExit();
				exit(-1);
				break;
			case -1:
				//do nothing
				updateView = true;
				break;
			default:
				//move to next frame
				updateView = false;
				break;
			}
		}
	}
}

void FernTarget::validateTargetView() {

	bool validatingTarget = true;
	while (validatingTarget && !targetValidated)
	{
		/* Show target candidate */
		if (!roiImg.empty()) {
			/* Show Image */
			cv::imshow(verificationWindowTitle, roiImg);
			/* Check for quit */
			char key = (char)cv::waitKey(1);
			switch (key) {
			case 'y' :
			case 'Y' :
				targetValidated = true;
				break;
			case 'q':
			case 'Q':
			case 27: //escape key
				validatingTarget = false;
				targetValidated = false;
				break;
			}
		}
	}

	cv::destroyAllWindows();
}
