#!/usr/bin/python

import glob
import cv2
import argparse

def displayTruth(path):

    folder = path.split("/")[-1]
    truthFile  = folder + '_bboxTruth.csv'

    if path[-1] != '/':
            path += '/'
    
    cv2.namedWindow('bbox View')
   
    # skip headder
    f_truth = open(path + truthFile)
    f_truth.next()

    for tline in f_truth:

	img, xt, yt, wt, ht = tline.split(',')
	
        xt = int(float(xt))
        yt = int(float(yt))
        wt = int(float(wt))
        ht = int(float(ht))

        imgStr = img
        img = cv2.imread(path + imgStr)
        cv2.rectangle(img, (xt, yt), (xt+wt, yt+ht), (0, 0, 255), 2)
        
        cv2.imshow('bbox View', img)
        cv2.waitKey(1)

    cv2.destroyWindow('truth data view')



def displayTruth_w_GOTURN(path):

    folder = path.split("/")[-1]
    truthFile  = folder + '_bboxTruth.csv'
    goturnFile = 'bboxOut.csv'

    if path[-1] != '/':
            path += '/'
    
    cv2.namedWindow('bbox View')
   
    # skip headder
    f_truth = open(path + truthFile)
    f_goturn = open(path + goturnFile)
    f_truth.next()
    f_goturn.next()

    for tline, gline in zip(f_truth, f_goturn):

	img, xt, yt, wt, ht = tline.split(',')
	xg, yg, wg, hg = gline.split(',')
	
        xt = int(float(xt))
        yt = int(float(yt))
        wt = int(float(wt))
        ht = int(float(ht))

        xg = int(float(xg))
        yg = int(float(yg))
        x2g = int(float(wg))
        y2g = int(float(hg))

        # print('Opening: {}\n'.format(path + img))
        
        imgStr = img
        img = cv2.imread(path + imgStr)
        cv2.rectangle(img, (xt, yt), (xt+wt, yt+ht), (255, 0, 0), 2)
        cv2.rectangle(img, (xg, yg), (x2g, y2g), (0, 255, 0), 2)
        
        cv2.imshow('bbox View', img)
        cv2.imwrite(path + '/out/' + imgStr, img)
        cv2.waitKey(1)

    cv2.destroyWindow('truth data view')


if __name__=='__main__':

    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("path")
    args = parser.parse_args()

    displayTruth(args.path)
