#=============================================================================
# Copyright 2017 FLIR Integrated Imaging Solutions, Inc. All Rights Reserved.
#
# This software is the confidential and proprietary information of FLIR
# Integrated Imaging Solutions, Inc. ("Confidential Information"). You
# shall not disclose such Confidential Information and shall use it only in
# accordance with the terms of the license agreement you entered into
# with FLIR Integrated Imaging Solutions, Inc. (FLIR).
#
# FLIR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
# SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE, OR NON-INFRINGEMENT. FLIR SHALL NOT BE LIABLE FOR ANY DAMAGES
# SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
# THIS SOFTWARE OR ITS DERIVATIVES.
#=============================================================================

import PyCapture2

def printBuildInfo():
	libVer = PyCapture2.getLibraryVersion()
	print "PyCapture2 library version: ", libVer[0], libVer[1], libVer[2], libVer[3]
	print

def printCameraInfo(cam):
	camInfo = cam.getCameraInfo()
	print "\n*** CAMERA INFORMATION ***\n"
	print "Serial number - ", camInfo.serialNumber
	print "Camera model - ", camInfo.modelName
	print "Camera vendor - ", camInfo.vendorName
	print "Sensor - ", camInfo.sensorInfo
	print "Resolution - ", camInfo.sensorResolution
	print "Firmware version - ", camInfo.firmwareVersion
	print "Firmware build time - ", camInfo.firmwareBuildTime
	print

def saveAviHelper(cam, fileFormat, fileName, frameRate):
	numImages = 100

	avi = PyCapture2.AVIRecorder()

	for i in range(numImages):
		try:
			image = cam.retrieveBuffer()
		except PyCapture2.Fc2error as fc2Err:
			print "Error retrieving buffer : ", fc2Err
			continue

		print "Grabbed image {}".format(i)

		if (i == 0):
			if fileFormat == "AVI":
				avi.AVIOpen(fileName, frameRate)
			elif fileFormat == "MJPG":
				avi.MJPGOpen(fileName, frameRate, 75)
			elif fileFormat == "H264":
				avi.H264Open(fileName, frameRate, image.getCols(), image.getRows(), 1000000)
			else:
				print "Specified format is not available."
				return

		avi.append(image)
		print "Appended image {}...".format(i)

	print "Appended {} images to {} file: {}...".format(numImages, fileFormat, fileName)
	avi.close()

#
# Example Main
#

# Print PyCapture2 Library Information
printBuildInfo()

# Ensure sufficient cameras are found
bus = PyCapture2.BusManager()
numCams = bus.getNumOfCameras()
print "Number of cameras detected: ", numCams
if not numCams:
	print "Insufficient number of cameras. Exiting..."
	exit()

# Select camera on 0th index
cam = PyCapture2.Camera()
cam.connect(bus.getCameraFromIndex(0))

# Print camera details
printCameraInfo(cam)

print "Starting capture..."
cam.startCapture()

print "Detecting frame rate from Camera"
fRateProp = cam.getProperty(PyCapture2.PROPERTY_TYPE.FRAME_RATE)
frameRate = fRateProp.absValue

print "Using frame rate of {}".format(frameRate)

for fileFormat in ("AVI","H264","MJPG"):
	fileName = "SaveImageToAviEx_{}.avi".format(fileFormat)
	saveAviHelper(cam, fileFormat, fileName, frameRate)

print "Stopping capture..."
cam.stopCapture()
cam.disconnect()

raw_input("Done! Press Enter to exit...\n")
