#=============================================================================
# Copyright 2017 FLIR Integrated Imaging Solutions, Inc. All Rights Reserved.
#
# This software is the confidential and proprietary information of FLIR
# Integrated Imaging Solutions, Inc. ("Confidential Information"). You
# shall not disclose such Confidential Information and shall use it only in
# accordance with the terms of the license agreement you entered into
# with FLIR Integrated Imaging Solutions, Inc. (FLIR).
#
# FLIR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
# SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE, OR NON-INFRINGEMENT. FLIR SHALL NOT BE LIABLE FOR ANY DAMAGES
# SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
# THIS SOFTWARE OR ITS DERIVATIVES.
#=============================================================================

import PyCapture2
from sys import exit
from time import sleep

def printBuildInfo():
	libVer = PyCapture2.getLibraryVersion()
	print "PyCapture2 library version: ", libVer[0], libVer[1], libVer[2], libVer[3]
	print

def printCameraInfo(cam):
	camInfo = cam.getCameraInfo()
	print "\n*** CAMERA INFORMATION ***\n"
	print "Serial number - ", camInfo.serialNumber
	print "Camera model - ", camInfo.modelName
	print "Camera vendor - ", camInfo.vendorName
	print "Sensor - ", camInfo.sensorInfo
	print "Resolution - ", camInfo.sensorResolution
	print "Firmware version - ", camInfo.firmwareVersion
	print "Firmware build time - ", camInfo.firmwareBuildTime
	print

def checkSoftwareTriggerPresence(cam):
	triggerInq = 0x530
	if(cam.readRegister(triggerInq) & 0x10000 != 0x10000):
		return False
	return True

def pollForTriggerReady(cam):
	softwareTrigger = 0x62C
	while True:
		regVal = cam.readRegister(softwareTrigger)
		if not regVal:
			break

def fireSoftwareTrigger(cam):
	softwareTrigger = 0x62C
	fireVal = 0x80000000
	cam.writeRegister(softwareTrigger, fireVal)

#
# Example Main
#

# Print PyCapture2 Library Information
printBuildInfo()

# Ensure sufficient cameras are found
bus = PyCapture2.BusManager()
numCams = bus.getNumOfCameras()
print "Number of cameras detected: ", numCams
if not numCams:
	print "Insufficient number of cameras. Exiting..."
	exit()

c = PyCapture2.Camera()
c.connect(bus.getCameraFromIndex(0))

# Power on the Camera
cameraPower = 0x610
powerVal = 0x80000000

c.writeRegister(cameraPower, powerVal)

# Waiting for camera to power up
retries = 10
timeToSleep = 0.1	#seconds
for i in range(retries):
	sleep(timeToSleep)
	try:
		regVal = c.readRegister(cameraPower)
	except PyCapture2.Fc2error:	# Camera might not respond to register reads during powerup.
		pass
	awake = True
	if regVal == powerVal:
		break
	awake = False
if not awake:
	print "Could not wake Camera. Exiting..."
	exit()

# Print camera details
printCameraInfo(c)

# Configure trigger mode
triggerMode = c.getTriggerMode()
triggerMode.onOff = True
triggerMode.mode = 0
triggerMode.parameter = 0
triggerMode.source = 7		# Using software trigger

c.setTriggerMode(triggerMode)

pollForTriggerReady(c)

c.setConfiguration(grabTimeout = 5000)

# Start acquisition
c.startCapture()

if not checkSoftwareTriggerPresence(c):
	print "SOFT_ASYNC_TRIGGER not implemented on this Camera! Stopping application"
	exit()

# Grab images
numImages = 10
for i in range(numImages):
	pollForTriggerReady(c)
	print "\nPress the Enter key to initiate a software trigger"
	raw_input()
	fireSoftwareTrigger(c)
	try:
		image = c.retrieveBuffer()
	except PyCapture2.Fc2error as fc2Err:
		print "Error retrieving buffer : ", fc2Err
		continue

	print "."

c.setTriggerMode(onOff = False)
print "Finished grabbing images!"

c.stopCapture()
c.disconnect()

raw_input("Done! Press Enter to exit...\n")
