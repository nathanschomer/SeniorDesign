#=============================================================================
# Copyright 2017 FLIR Integrated Imaging Solutions, Inc. All Rights Reserved.
#
# This software is the confidential and proprietary information of FLIR
# Integrated Imaging Solutions, Inc. ("Confidential Information"). You
# shall not disclose such Confidential Information and shall use it only in
# accordance with the terms of the license agreement you entered into
# with FLIR Integrated Imaging Solutions, Inc. (FLIR).
#
# FLIR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
# SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE, OR NON-INFRINGEMENT. FLIR SHALL NOT BE LIABLE FOR ANY DAMAGES
# SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
# THIS SOFTWARE OR ITS DERIVATIVES.
#=============================================================================

import PyCapture2

def printBuildInfo():
	libVer = PyCapture2.getLibraryVersion()
	print "PyCapture2 library version: ", libVer[0], libVer[1], libVer[2], libVer[3]
	print

def printCameraInfo(cam):
	camInfo = cam.getCameraInfo()
	print "\n*** CAMERA INFORMATION ***\n"
	print "Serial number - ", camInfo.serialNumber
	print "Camera model - ", camInfo.modelName
	print "Camera vendor - ", camInfo.vendorName
	print "Sensor - ", camInfo.sensorInfo
	print "Resolution - ", camInfo.sensorResolution
	print "Firmware version - ", camInfo.firmwareVersion
	print "Firmware build time - ", camInfo.firmwareBuildTime

def enableEmbeddedTimeStamp(cam, enableTimeStamp):
	embeddedInfo = cam.getEmbeddedImageInfo()
	if embeddedInfo.available.timestamp:
		cam.setEmbeddedImageInfo(timestamp = enableTimeStamp)
		if(enableTimeStamp):
			print "\nTimeStamp is enabled.\n"
		else:
			print "\nTimeStamp is disabled.\n"

def grabImages(cam, numImagesToGrab):
	prevts = None
	imgList = []
	for i in xrange(numImagesToGrab):
		image = cam.retrieveBuffer()
		ts = image.getTimeStamp()
		if (prevts):
			diff = (ts.cycleSeconds - prevts.cycleSeconds) * 8000 + (ts.cycleCount - prevts.cycleCount)
			print "Timestamp [", ts.cycleSeconds, ts.cycleCount, "] -", diff
		prevts = ts

	print "\nPerforming statistics calculation..."
	testimg = image.convert(PyCapture2.PIXEL_FORMAT.MONO8)
	stats = PyCapture2.ImageStatistics()
	stats.enableGreyChannel()
	stats.calculateStatistics(testimg)
	channelLookup = {'GREY': 0,'RED': 1, 'GREEN': 2, 'BLUE': 3, 'HUE': 4, 'SATURATION': 5, 'LIGHTNESS': 6}
	for key, value in channelLookup.iteritems():
		if not stats.getChannelStatus(value):
			print key, "is not enabled."
		else:
			print key, ":"
			range, valRange, numVals, mean, histo = stats.getStatistics(value)
			print """Range:			{}	to	{}
Value Range:		{}	to	{}
Number of Values:	{}
Mean value:			{}
Histogram:
{}
	""".format(range[0], range[1], valRange[0], valRange[1], numVals, mean, ", ".join([str(hist) for hist in histo]))

#
# Example Main
#

# Print PyCapture2 Library Information
printBuildInfo()

# Ensure sufficient cameras are found
bus = PyCapture2.BusManager()
numCams = bus.getNumOfCameras()
print "Number of cameras detected: ", numCams
if not numCams:
	print "Insufficient number of cameras. Exiting..."
	exit()

# Select camera on 0th index
c = PyCapture2.Camera()
uid = bus.getCameraFromIndex(0)
c.connect(uid)

# Print camera details
printCameraInfo(c)

# Enable camera embedded timestamp
enableEmbeddedTimeStamp(c, True)

print "Starting image capture..."
c.startCapture()
grabImages(c, 10)
c.stopCapture()

# Disable camera embedded timestamp
enableEmbeddedTimeStamp(c, False)
c.disconnect()

raw_input("Done! Press Enter to exit...\n")
