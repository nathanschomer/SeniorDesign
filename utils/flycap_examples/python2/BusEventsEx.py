#=============================================================================
# Copyright 2017 FLIR Integrated Imaging Solutions, Inc. All Rights Reserved.
#
# This software is the confidential and proprietary information of FLIR
# Integrated Imaging Solutions, Inc. ("Confidential Information"). You
# shall not disclose such Confidential Information and shall use it only in
# accordance with the terms of the license agreement you entered into
# with FLIR Integrated Imaging Solutions, Inc. (FLIR).
#
# FLIR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
# SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE, OR NON-INFRINGEMENT. FLIR SHALL NOT BE LIABLE FOR ANY DAMAGES
# SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
# THIS SOFTWARE OR ITS DERIVATIVES.
#=============================================================================

import PyCapture2
from time import time

def printBuildInfo():
	libVer = PyCapture2.getLibraryVersion()
	print "PyCapture2 library version: ", libVer[0], libVer[1], libVer[2], libVer[3]
	print

def onBusReset(serialNum):
	print time(), "- *** BUS RESET ***"

def onBusArrival(serialNum):
	print time(), "- *** BUS ARRIVAL ***"

def onBusRemoval(serialNum):
	print time(), "- *** BUS REMOVAL ***"

#
# Example Main
#

# Print PyCapture2 Library Information
printBuildInfo()

# Register bus arrival/removal/reset callbacks
bus = PyCapture2.BusManager()
bus.registerCallback(PyCapture2.BUS_CALLBACK_TYPE.BUS_RESET, onBusReset)
bus.registerCallback(PyCapture2.BUS_CALLBACK_TYPE.ARRIVAL, onBusArrival)
bus.registerCallback(PyCapture2.BUS_CALLBACK_TYPE.REMOVAL, onBusRemoval)

raw_input("Done! Press Enter to exit...\n")

# Un-register bus arrival/removal/reset callbacks
bus.unregisterCallback(PyCapture2.BUS_CALLBACK_TYPE.BUS_RESET)
bus.unregisterCallback(PyCapture2.BUS_CALLBACK_TYPE.ARRIVAL)
bus.unregisterCallback(PyCapture2.BUS_CALLBACK_TYPE.REMOVAL)
