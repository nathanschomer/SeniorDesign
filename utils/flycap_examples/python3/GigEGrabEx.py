#=============================================================================
# Copyright 2017 FLIR Integrated Imaging Solutions, Inc. All Rights Reserved.
#
# This software is the confidential and proprietary information of FLIR
# Integrated Imaging Solutions, Inc. ("Confidential Information"). You
# shall not disclose such Confidential Information and shall use it only in
# accordance with the terms of the license agreement you entered into
# with FLIR Integrated Imaging Solutions, Inc. (FLIR).
#
# FLIR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
# SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE, OR NON-INFRINGEMENT. FLIR SHALL NOT BE LIABLE FOR ANY DAMAGES
# SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
# THIS SOFTWARE OR ITS DERIVATIVES.
#=============================================================================

import PyCapture2
from sys import exit

def printBuildInfo():
	libVer = PyCapture2.getLibraryVersion()
	print("PyCapture2 library version: ", libVer[0], libVer[1], libVer[2], libVer[3])
	print()

def printCameraInfo(camInfo):
	print("\n*** CAMERA INFORMATION ***\n")
	print("Serial number - ", camInfo.serialNumber)
	print("Camera model - ", camInfo.modelName)
	print("Camera vendor - ", camInfo.vendorName)
	print("Sensor - ", camInfo.sensorInfo)
	print("Resolution - ", camInfo.sensorResolution)
	print("Firmware version - ", camInfo.firmwareVersion)
	print("Firmware build time - ", camInfo.firmwareBuildTime)
	print()
	print("GigE major version - ", camInfo.gigEMajorVersion)
	print("GigE minor version - ", camInfo.gigEMinorVersion)
	print("User-defined name - ", camInfo.userDefinedName)
	print("XML URL1 - ", camInfo.xmlURL1)
	print("XML URL2 - ", camInfo.xmlURL2)
	print("MAC address - ", camInfo.macAddress[0], camInfo.macAddress[1], camInfo.macAddress[2], camInfo.macAddress[3], camInfo.macAddress[4], camInfo.macAddress[5])
	print("IP address - ", camInfo.ipAddress[0], camInfo.ipAddress[1], camInfo.ipAddress[2], camInfo.ipAddress[3])
	print("Subnet mask - ", camInfo.subnetMask[0], camInfo.subnetMask[1], camInfo.subnetMask[2], camInfo.subnetMask[3])
	print("Default geteway - ", camInfo.defaultGateway[0], camInfo.defaultGateway[1], camInfo.defaultGateway[2], camInfo.defaultGateway[3])
	print()

def printStreamChannelInfo(streamInfo):
	print("Network interface: ", streamInfo.networkInterfaceIndex)
	print("Host port: ", streamInfo.hostPort)
	print("Do not fragment bit: ", "Enabled" if streamInfo.doNotFragment else "Disabled")
	print("Packet size: ", streamInfo.packetSize)
	print("Inter-packet delay: ", streamInfo.interPacketDelay)
	print("Destination IP address: ", streamInfo.destinationIpAddress[0], streamInfo.destinationIpAddress[1], streamInfo.destinationIpAddress[2], streamInfo.destinationIpAddress[3])
	print("Source port (on Camera): ", streamInfo.sourcePort)
	print()

def enableEmbeddedTimeStamp(cam, enableTimeStamp):
	embeddedInfo = cam.getEmbeddedImageInfo()
	if embeddedInfo.available.timestamp:
		cam.setEmbeddedImageInfo(timestamp = enableTimeStamp)
		if(enableTimeStamp):
			print("\nTimeStamp is enabled.\n")
		else:
			print("\nTimeStamp is disabled.\n")		
	
def runSingleCamera(cam, uID):
	print("Connecting to Camera...")
	cam.connect(uID)
	printCameraInfo(cam.getCameraInfo())
	for i in range(cam.getNumStreamChannels()):
		printStreamChannelInfo(cam.getGigEStreamChannelInfo(i))
	print("Querying GigE image setting information...")
	imgSetInfo = cam.getGigEImageSettingsInfo()
	imgSet = PyCapture2.GigEImageSettings()
	
	imgSet.offsetX = 0
	imgSet.offsetY = 0
	imgSet.height = imgSetInfo.maxHeight
	imgSet.width = imgSetInfo.maxWidth
	imgSet.pixelFormat = PyCapture2.PIXEL_FORMAT.MONO8
	
	print("Setting GigE image settings...")
	cam.setGigEImageSettings(imgSet)
	enableEmbeddedTimeStamp(cam, True)
	
	print("Starting image capture...")
	cam.startCapture()
	prevts = None
	numImagesToGrab = 10
	for i in range(numImagesToGrab):
		try:
			image = cam.retrieveBuffer()
		except PyCapture2.Fc2error as fc2Err:
			print("Error retrieving buffer : ", fc2Err)
			continue

		ts = image.getTimeStamp()
		if (prevts):
			diff = (ts.cycleSeconds - prevts.cycleSeconds) * 8000 + (ts.cycleCount - prevts.cycleCount)
			print("Timestamp [", ts.cycleSeconds, ts.cycleCount, "] -", diff)
		prevts = ts
		
	newimg = image.convert(PyCapture2.PIXEL_FORMAT.BGR)
	print("Saving the last image to GigEGrabEx.png")
	newimg.save("GigEGrabEx.png".encode("utf-8"), PyCapture2.IMAGE_FILE_FORMAT.PNG)
	cam.stopCapture()
	enableEmbeddedTimeStamp(cam, False)
	cam.disconnect()

#
# Example Main
#

# Print PyCapture2 Library Information
printBuildInfo()

# Ensure sufficient cameras are found
bus = PyCapture2.BusManager()
camInfos = bus.discoverGigECameras()
for ci in camInfos:
	printCameraInfo(ci)

if not len(camInfos):
	print("No suitable GigE cameras found. Exiting...")
	exit()

# Run example on all cameras
cam = PyCapture2.GigECamera()
for i in range(bus.getNumOfCameras()):
	uID = bus.getCameraFromIndex(i)

	ifaceType = bus.getInterfaceTypeFromGuid(uID)
	if ifaceType == PyCapture2.INTERFACE_TYPE.GIGE:
		runSingleCamera(cam, uID)

input("Done! Press Enter to exit...\n")
