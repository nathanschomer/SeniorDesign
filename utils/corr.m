clear all; close all; clc;

cd /home/nathan/data/new_data/
dirList = split(ls);
dirSize = size(dirList, 1)-1;

rslt = zeros(4, 10, dirSize);
rsltAvg = zeros(4, 10);
cnt = 0;

selection = [];
errSum = [];
corrArray = [];
imuMag = {};
errVals = [];

test_set1 = {'lid5', 'soap2', 'vinyl3', 'box7', 'ball1', 'box3'};
test_set2 = {'aloe2', 'aloe5', 'cube6', 'cube7', 'cube9', 'vinyl2', 'vinyl3', 'vinyl4'};
test_set3 = {'aloe5'}

improved = {};
worse = {};
same = {};

for d = 1:dirSize
    
%     if ~any(strcmp(test_set3, dirList{d}))
%         continue;
%     end
    
    currDir = dirList{d};
    %fprintf("Opening %s...\n", currDir);
    
    % get bbox error and imu data
    mBboxErr = getBboxErr(currDir, 'mm_orig_bboxOut.csv');
    gBboxErr = getBboxErr(currDir, 'orig_bboxOut.csv');
    
    frameNum = (size(mBboxErr, 1):-1:1)'.^2;
    imuMag{d, 1} = currDir;
    imuMag{d, 2} = getIMU(currDir);
    
    % currError = cpDist * bboxArea * frameNum^2
    % currError = 
    %mErr = mBboxErr(:,2).*mBboxErr(:,4).*mBboxErr(:, 3).*((size(mBboxErr, 1):-1:1)');
    %gErr = gBboxErr(:,2).*gBboxErr(:,4).*gBboxErr(:, 3).*((size(gBboxErr, 1):-1:1)');
    
    a = 1;
    b = 0.5;
    mFrameWeights = linspace(a, b, size(mBboxErr, 1));
    gFrameWeights = linspace(a, b, size(gBboxErr, 1));
    
    mErr = mBboxErr(:,5).*mBboxErr(:,3).*mFrameWeights';
    gErr = gBboxErr(:,5).*gBboxErr(:,3).*gFrameWeights';
    
%     figure;
%     plot(mErr);
%     hold on;
%     plot(gErr);
%     title(['Error Comparison of ', dirList{d}]);
%     legend('Predictive Motion Model', 'Original GOTURN');
%     xlabel('Frame Number');
%     ylabel('Error Value');
%     pause;
    
    errVals = [errVals; sum(gErr), sum(mErr), sum(gErr)-sum(mErr)];
    
    %errSum = [errSum; currErr];
    % figure;
    % plot(gErr);
    % hold on;
    % plot(mErr);
    % legend('gErr', 'mErr');
    % title(strcat( dirList{d}, " Error" ));

    %fprintf("Set: %s, Error Val: %f\n", dirList{d}, errSum);

end

for n=1:size(errVals, 1)
    if (errVals(n, 1) > errVals(n, 2))
        improved(size(improved, 1)+1, 1) = dirList(n);
    elseif (errVals(n, 1) < errVals(n, 2))
        worse(size(worse, 1)+1, 1) = dirList(n);
    else
        same(size(same, 1)+1, 1) = dirList(n);
    end
    
end

fprintf("\nRESUTLS:\n\tImproved: %d\n\tWorse: %d\n", size(improved, 1), size(worse, 1));

figure;
histogram(errVals(:, 3), 20);

figure;
bar(errVals(:, 3));
%set(gca,'xticklabel',dirList);


function corr=getCorrelation(bboxErr, imuData)
    corr = zeros(size(bboxErr, 2), size(imuData, 2));
    minDim = min(size(bboxErr, 1), size(imuData, 1));
    
    for j = 1:size(bboxErr, 2)
        for k = 1:size(imuData, 2)
            tmp = corrcoef(bboxErr(1:minDim, j), imuData(1:minDim, k));
            corr(j, k) = tmp(2);
        end
    end
end

function imuMag=getIMU(myDir)
    
    fid = fopen(strcat(myDir, "/", myDir, "_imu.csv"));
    fgetl(fid);
    imuData = textscan(fid, '%s %f %f %f %f %f %f %f %f %f %f', 'Delimiter',',', 'HeaderLines',0, 'CollectOutput',1);
    fclose(fid);
    
    deltas = [0, 0, 0, 0];
    
    % array entries: (delta_x, delta_y, delta_z, delta_w)
    for n = 2:size(imuData{2}, 1)
        delta = (imuData{2}(n, 1:4) - imuData{2}(n-1, 1:4)).^2; % * 10^6;
        %mag = delta(1) * delta(2) * delta(3) * delta(4);
        deltas = [deltas; delta];
    end
    
    imuMag = sum(sum(deltas));
    
    %for n=1:size(data{2}, 2)
    %    data{2}(:, n) = data{2}(:f,n) - mean(data{2}(:,n));
    %end

    %for n=1:size(data{2}, 1)
    %    imuMag(n) = sum(data{2}(n, :).^2);
    %end
    
end

function bboxErr=getBboxErr(myDir, filename)
    
    % calculate error between truth and GOTURN labels
    truth_fid = fopen(strcat(myDir, "/", myDir, "_bboxTruth.csv"));
    goturn_fid = fopen(strcat(myDir, "/", filename));
    
    fgetl(truth_fid);
    fgetl(goturn_fid);
    
    tdata = textscan(truth_fid, '%s %f %f %f %f', 'Delimiter', ',', 'HeaderLines', 0, 'CollectOutput', 1);
    gdata = textscan(goturn_fid, '%f %f %f %f', 'Delimiter', ',', 'HeaderLines', 0, 'CollectOutput', 1); 
    
    for n = 1:min(size(tdata{1, 2}, 1), size(gdata{1, 1}, 1))
         
        tw = tdata{1, 2}(n, 3);
        th = tdata{1, 2}(n, 4);
        tx = tdata{1, 2}(n, 1) + tw;
        ty = tdata{1, 2}(n, 2) + th;
         
        gw = gdata{1, 1}(n, 3);
        gh = gdata{1, 1}(n, 4);
        gx = gdata{1, 1}(n, 1) + gw;
        gy = gdata{1, 1}(n, 2) + gh;

        % bbox overlap between truth and output
        bboxErr(n, 1) = 0; %rectint(tdata{1, 2}(n, 1:4), gdata{1, 1}(n, 1:4)) / min(tw*th, gw*gh);
         
        % distance between centerpoints
        bboxErr(n, 2) = sqrt((tx-gx)^2 +(ty-gy)^2);
        
        % area of truth bbox
        bboxErr(n, 3) = (tw * th) / (480 * 640);
        
        % set error to zero if centerpoint is within truth bbox
        if( (gx >= tx-tw) && (gy >= ty-th) && (gx <= tx+tw) && (gy <= ty+th) )
            bboxErr(n, 4) = 0;
        else
            bboxErr(n, 4) = 1;
        end
        
        bboxErr(n, 5) = sqrt((tx-gx)^2 +(ty-gy)^2) / sqrt(480^2+640^2);
        
    end
end