#!/usr/bin/python

import glob
import cv2
import argparse

def displayTruth(path):

    folder = path.split("/")[-1]
    truthFile = folder + '_bboxTruth.csv'
    new_truthFile = folder + '_bboxTruth.csv.new'

    if path[-1] != '/':
            path += '/'
    
    # skip headder
    f = open(path + truthFile, 'r')
    f_new = open(path + new_truthFile, 'w')

    # write header to new file
    f_new.write( f.readline() )

    # last bounding box
    x_old, y_old, w_old, h_old = None, None, None, None;

    THRESH = 0.1 #10 % overlap threshold

    for line in f:

        # get current bbox
	img, x_str, y_str, w_str, h_str = line.split(',')
	
        x = int(float(x_str))
        y = int(float(y_str))
        w = int(float(w_str))
        h = int(float(h_str))

        if x_old is not None:
           
            # calculate % overlap or some other shit
            # TODO: DOUBLE CHECK THESE CALCULATIONS
            overlap_x = ((x_old + w) - x)
            overlap_y = ((y_old + h) - y)

            if overlap_x < 0:
                overlap_x = overlap_x * -1

            if overlap_y < 0:
                overlap_y = overlap_y * -1

            overlapArea = overlap_x * overlap_y
            totalArea = w_old*h_old + w*h
            overlap = overlapArea / totalArea
            
            if overlap < thresh:

                # WARNING: w & h MIGHT BE BACKWARDS
                x = x + w
                y = y + h

                # build new_line and write to file
                newline = '{},{},{},{},{}'.format(img, x, y, w, h)
                f_new.write( newline );
            
            else:
                # write current line to file
                f_new.write( line );

        # store current as old
        x_old, y_old, w_old, h_old = x, y, w, h

    # close files before returning
    f.close()
    f_new.close()


if __name__=='__main__':

    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("path")
    args = parser.parse_args()

    displayTruth(args.path)

