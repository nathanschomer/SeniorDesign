#! /usr/bin/python

import argparse
import os
import numpy as np

import matplotlib.pyplot as plt

def testRead(basePath, folder):

    f_imu = open(os.path.join(basePath, folder, folder+"_imu.csv"), 'r')
    f_imu.readline()
    imuData = [line.split(',') for line in f_imu.readlines()]
    imuData = [[int(line[0][3:])] + [float(elem) for elem in line[1:]] for line in imuData]

    f_truth = open(os.path.join(basePath, folder, folder+"_bboxSmoothed.csv"))
    f_truth.readline()
    truthData = [line.split(',') for line in f_truth.readlines()]
    truthData = [[int(line[0][3:-4])] + [float(elem) for elem in line[1:]] for line in truthData]

    startFrame = max(truthData[0][0], imuData[0][0])
    endFrame = min(truthData[-1][0], imuData[-1][0])
   
    imuData = [line[1:] for line in imuData if (line[0] >= startFrame and line[0] <= endFrame)]
    truthData = [line[1:] for line in truthData if (line[0] >= startFrame and line[0] <= endFrame)]

    if len(imuData) != len(truthData):
        print('\n{}'.format(folder))
        print(len(imuData))
        print(len(truthData))

    deltas = [[0, 0]]
    areas = []

    x = y = 0

    for n, line in enumerate(truthData):
        
        xOld, yOld = x, y
        x, y, w, h = line
        areas.append( (w*h) / (640*480) )

        if n > 0:
            deltas.append([x-xOld, y-yOld])
       
    return imuData, deltas, areas

def getIMU(basePath, folder):
    
    f_imu = open(os.path.join(basePath, folder, folder+"_imu.csv"), 'r')
    f_imu.readline()

    imuData = []

    for n, imuLine in enumerate(f_imu):
        imuData.append([float(x) for x in imuLine.split(',')[1:]])

    return n, imuData


def getArea(basePath, folder):

    path = os.path.join(basePath, folder, folder+"_bboxSmoothed.csv")
    tmp = np.genfromtxt(path, delimiter=',', skip_header=1, usecols=(range(3,5)))
    areas = np.multiply(tmp[:,0], tmp[:, 1])
    areas = np.divide(areas, (640*480))
    return areas

def getDeltas(basePath, folder):

    path = os.path.join(basePath, folder, folder+"_bboxSmoothed.csv")
    pos = np.genfromtxt(path, delimiter=',', skip_header=1, usecols=(range(1,3)))

    deltas = np.zeros([1, 2], dtype=np.float64)
    deltas = np.concatenate((deltas, np.diff(pos, axis=0)), axis=0)

    return deltas

def getVel(basePath, folder, shift):

    path = os.path.join(basePath, folder, folder+"_imu.csv")
    pos = np.genfromtxt(path, delimiter=',', skip_header=1, usecols=(range(1,5)))
   
    maxLen = pos.shape[0]

    vel = np.zeros([shift+1, 4], dtype=np.float64)
    vel = np.concatenate((vel, np.diff(pos, axis=0)), axis=0)
    vel = vel[0:maxLen, :]

    return vel


def getPos(basePath, folder):

    f_truth = open(os.path.join(basePath, folder, folder+"_bboxSmoothed.csv"))
    f_truth.readline() 
    
    deltas = []
    areas = []

    x = y = 0
    
    for n, line in enumerate(f_truth):
        
        old_x, old_y = x, y
        x, y, w, h = [float(x) for x in line.split(',')[1:]]
       
        areas.append( (x*y) / (640*480) )

        if n > 0:
            # (current - previous) ...STICK TO THIS CONVENTION!!!
            deltas.append([x - old_x, y - old_y])

    return n, deltas, areas


def dealWithArgs():
    """
    Handle input command line input arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('dataPath')
    return parser.parse_args()

if __name__=='__main__':
    
    args = dealWithArgs()
    dirs = os.listdir(args.dataPath)

    vel = None
    delats = None
    areas = None

    SHIFT = 100

    dirlist = []

    for d in dirs:

        print(d)
        dirlist.append(d)

        tmpVel = getVel(args.dataPath, d, SHIFT)
        tmpDelta = getDeltas(args.dataPath, d)
        tmpArea = getArea(args.dataPath, d)

        if d is not dirs[0]:
            vel = np.concatenate((vel, tmpVel))
            deltas = np.concatenate((deltas, tmpDelta))
            areas = np.concatenate((areas, tmpArea))
        else:
            vel = tmpVel
            deltas = tmpDelta
            areas = tmpArea

    print('deltas.shape = {}'.format(deltas.shape))
    print('vel.shape = {}'.format(vel.shape))
    print('area.shape = {}'.format(areas.shape))
