clear; close all; clc;

cd /home/nathan/data/new_data/
%cd /media/nathan/ae8bd8ad-5f50-4c99-a9da-ae8d611b3909/extern/data/

deltaPath = '/home/nathan/xydiff.csv'
deltaVals = csvread(deltaPath)

dirList = split(ls);
dirSize = size(dirList, 1)-1;

rslt = zeros(5, 10, dirSize);

rsltAvg = zeros(5, 10);
cnt = 0;

allIMU = {};
allBbox = {};

corrCell = {};

posX = [];
posY = [];
posZ = [];
posW = [];

deltas = [];

imuData = [];

for d = 1:dirSize 
    
    currDir = dirList{d};
    imuData = getIMU(currDir);
    
    deltas = [deltas; getDeltas(currDir)];
    posX = [posX; imuData{1, 2}(:, 1)];
    posY = [posX; imuData{1, 2}(:, 2)];
    posZ = [posX; imuData{1, 2}(:, 3)];
    posW = [posX; imuData{1, 2}(:, 4)];
    
    %posY{d} = imuData{1, 2}(:, 2);
    %posZ{d} = imuData{1, 2}(:, 3);
    %posW{d} = imuData{1, 2}(:, 4);
  
%         fprintf("Opening %s...\n", currDir);
%         
%         bboxErr = getBboxErr(currDir);
%         imuData = getIMU(currDir);
%         
%         minDim = min(size(bboxErr, 1), size(imuData{1, 2}, 1));
%         
%         posX = [posX imuData{1, 2}(1:minDim, 1)];
%         posY = [posY imuData{1, 2}(1:minDim, 2)];
%         posZ = [posZ imuData{1, 2}(1:minDim, 3)];
%         posW = [posW imuData{1, 2}(1:minDim, 4)];
%         
%         accelX = [ accelX imuData{1, 2}(1:minDim, 8)];
%         accelY = [accelY imuData{1, 2}(1:minDim, 9)];
%         accelZ = [accelZ imuData{1, 2}(1:minDim, 10)];
%         
%         accelCorrX = abs(corrcoef(bboxErr(1:minDim, 2), imuData{1, 2}(1:minDim, 8)));
%         accelCorrY = abs(corrcoef(bboxErr(1:minDim, 2), imuData{1, 2}(1:minDim, 9)));
%         accelCorrZ = abs(corrcoef(bboxErr(1:minDim, 2), imuData{1, 2}(1:minDim, 10)));
%         
%         corrCell{d, 1} = currDir;
%         corrCell{d, 2} = accelCorrX(2);
%         corrCell{d, 3} = accelCorrY(2);
%         corrCell{d, 4} = accelCorrZ(2);
%         corrCell{d, 5} = mean([accelCorrX(2) accelCorrY(2) accelCorrZ(2)]);
%         corrCell{d, 6} = max([corrCell{d, 2:5}]);;        
end


function corr=getCorrelation(bboxErr, imuData)
    corr = zeros(size(bboxErr, 2), size(imuData, 2));
    minDim = min(size(bboxErr, 1), size(imuData, 1));
    
    for j = 1:size(bboxErr, 2)
        for k = 1:size(imuData, 2)
            tmp = corrcoef(bboxErr(1:minDim, j), imuData(1:minDim, k));
            corr(j, k) = tmp(2);
        end
    end
end

function imuData=getIMU(myDir)
    
    fid = fopen(strcat(myDir, "/", myDir, "_imu.csv"));
    fgetl(fid);
    imuData = textscan(fid, '%s %f %f %f %f %f %f %f %f %f %f', 'Delimiter',',', 'HeaderLines',0, 'CollectOutput',1);
    fclose(fid);
    
end

function vals=getDeltas(myDir)
    
    truth_fid = fopen(strcat(myDir, "/", myDir, "_bboxSmoothed.csv"));
    fgetl(truth_fid);
    tdata = textscan(truth_fid, '%s %f %f %f %f', 'Delimiter', ',', 'HeaderLines', 0, 'CollectOutput', 1);
    
    for n = 2:size(tdata{1, 2}, 1)
        vals(n, 1) = tdata{1, 2}(n, 1) - tdata{1, 2}(n-1, 1);
        vals(n, 2) = tdata{1, 2}(n, 2) - tdata{1, 2}(n-1, 2);
    end
    
    
end

function bboxErr=getBboxErr(myDir)
    
    % calculate error between truth and GOTURN labels
    truth_fid = fopen(strcat(myDir, "/", myDir, "_bboxSmoothed.csv"));
    goturn_fid = fopen(strcat(myDir, "/bboxOut.csv"));
    
    fgetl(truth_fid);
    fgetl(goturn_fid);
    
    tdata = textscan(truth_fid, '%s %f %f %f %f', 'Delimiter', ',', 'HeaderLines', 0, 'CollectOutput', 1);
    gdata = textscan(goturn_fid, '%f %f %f %f', 'Delimiter', ',', 'HeaderLines', 0, 'CollectOutput', 1); 
    
    for n = 1:min(size(tdata{1, 2}, 1), size(gdata{1, 1}, 1))
         

        tw = tdata{1, 2}(n, 3);
        th = tdata{1, 2}(n, 4);
        tx = tdata{1, 2}(n, 1);
        ty = tdata{1, 2}(n, 2);
         
        gw = gdata{1, 1}(n, 3);
        gh = gdata{1, 1}(n, 4);
        gx = gdata{1, 1}(n, 1);
        gy = gdata{1, 1}(n, 2);

        % overlap 
        bboxErr(n, 1) = 1-(rectint([gx, gy, gw, gh], [tx, ty, tw, th]) / min(gw*gh, th*tw));
         
        % distance between centerpoints divided by 
        bboxErr(n, 2) = sqrt((tx-gx)^2 +(ty-gy)^2);
        
        % area of truth bbox
        bboxErr(n, 3) = gw * gh;
        
        % area of goturn output bbox
        bboxErr(n, 4) = tw * th;
        
        a = 1.121e-195;
        b = 448.9;
        overlapMiss = bboxErr(n, 1);
        dist = bboxErr(n, 2);
        
        truthDiag = sqrt(tw^2 + th^2);
        bboxErr(n, 5) = min(1, a*exp(b*overlapMiss)) * (dist/truthDiag);
    end
    
end