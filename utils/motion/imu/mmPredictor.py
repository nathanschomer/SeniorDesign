#! /usr/bin/python

import os
import lmdb
import caffe
import argparse
import numpy as np
from tqdm import tqdm


def testRead(basePath, folder):

	f_imu = open(os.path.join(basePath, folder, folder+"_imu.csv"), 'r')
	f_imu.readline()
	imuData = [line.split(',') for line in f_imu.readlines()]
	imuData = [[int(line[0][3:])] + [float(elem) for elem in line[1:]] for line in imuData]

	f_truth = open(os.path.join(basePath, folder, folder+"_bboxSmoothed.csv"))
	f_truth.readline()
	truthData = [line.split(',') for line in f_truth.readlines()]
	truthData = [[int(line[0][3:-4])] + [float(elem) for elem in line[1:]] for line in truthData]

	startFrame = max(truthData[0][0], imuData[0][0])
	endFrame = min(truthData[-1][0], imuData[-1][0])
   
	imuData = [line[1:] for line in imuData if (line[0] >= startFrame and line[0] <= endFrame)]
	truthData = [line[1:] for line in truthData if (line[0] >= startFrame and line[0] <= endFrame)]

	if len(imuData) != len(truthData):
		print('\n{}'.format(folder))
		print(len(imuData))
		print(len(truthData))

	deltas = [[0, 0]]
	areas = []

	x = y = 0

	for n, line in enumerate(truthData):
		
		x, y, w, h = line

		areas.append( (w*h) / (640*480) )

		if n > 0:
			deltas.append([x-xOld, y-yOld])
		xOld, yOld = x, y
	   
	return imuData, deltas, areas

def getIMU(basePath, folder):
	
	f_imu = open(os.path.join(basePath, folder, folder+"_imu.csv"), 'r')
	f_imu.readline()

	imuData = []

	for n, imuLine in enumerate(f_imu):
		imuData.append([float(x) for x in imuLine.split(',')[1:]])

	return n, imuData


def getPos(basePath, folder):

	f_truth = open(os.path.join(basePath, folder, folder+"_bboxSmoothed.csv"))
	f_truth.readline() 
	
	deltas = []
	areas = []

	x = y = 0
	
	for n, line in enumerate(f_truth):
		
		old_x, old_y = x, y
		x, y, w, h = [float(x) for x in line.split(',')[1:]]
	   
		areas.append( (x*y) / (640*480) )

		if n > 0:
			# (current - previous) ...STICK TO THIS CONVENTION!!!
			deltas.append([x - old_x, y - old_y])

	return n, deltas, areas


def dealWithArgs():
	"""
	Handle input command line input arguments
	"""
	parser = argparse.ArgumentParser()
	parser.add_argument('dataPath')
	return parser.parse_args()

if __name__=='__main__':
	
	args = dealWithArgs()
	dirs = os.listdir(args.dataPath)

	imu = []
	deltas = []
	areas = []

	for currDir in dirs:
		_imu, _deltas, _areas = testRead(args.dataPath, currDir)
		imu = imu + _imu
		deltas = deltas + _deltas
		areas = areas + _areas
	
	print(len(imu))
	print(len(deltas))
	print(len(areas))
	#print(deltas)


	buf=10000000000
	env=lmdb.open('imuScale',map_size=buf)
	max_key = env.stat()["entries"]
	print 'There are currently ___ entries in the lmdb'
	print max_key

	with env.begin(write=True) as txn:
		for i in tqdm(range(len(imu))):
			#print label[i][0]
			datum=caffe.proto.caffe_pb2.Datum()
			datum.channels=10
			datum.height=1
			datum.width=1
			imu[i][9]=areas[i]
			datum.float_data.extend(np.array(imu[i]).flat)
			str_id='{:80}'.format(i+max_key+1)

			txn.put(str_id.encode('ascii'),datum.SerializeToString())

	env=lmdb.open('labels',map_size=buf)
	with env.begin(write=True) as txn:
		for i in tqdm(range(len(deltas))):
			datum=caffe.proto.caffe_pb2.Datum()
			datum.channels=2
			datum.height=1
			datum.width=1
			datum.float_data.extend(np.array(deltas[i]).flat)
			#datum.label=np.array([ int(num) for num in label[i]])
			str_id='{:80}'.format(i+max_key+1)

			txn.put(str_id.encode('ascii'),datum.SerializeToString())


