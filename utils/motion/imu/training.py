#!/usr/bin/python
import os
import sys
import csv
import pickle
import argparse as AP

import cv2
import lmdb
import caffe

#import Image
import numpy as np
caffe_root = '/opt/caffe'

def extractArgs():
	parse=AP.ArgumentParser(description='',epilog='')

	imgArgs=parse.add_argument_group('Image Arguments')
	imgArgs.add_argument('-w',default=227,type=int,help='Width of output image, default is 227px')
	imgArgs.add_argument('-l',default=227,type=int,help='Height of output image, default is 227px')

	orgArgs=parse.add_argument_group('Organizational Arguments')
	orgArgs.add_argument('--dataDir',default='./data/',help='Directory to pull pictures from')
	orgArgs.add_argument('--TRAINDIR',default='./train/',help='Directory to save training data')
	orgArgs.add_argument('--VALDIR',default='./val/',help='Directory to save validation data')
	orgArgs.add_argument('--toLMDB',action='store_true',help='Save instead to an lmdb file')
	orgArgs.add_argument('--PreProc',action='store_true',help='Just pre-process the data and make the ImageList.pkl file')

	return parse.parse_args()



if __name__ == '__main__':
	#Read command line arguments
	args=extractArgs()

	caffe.set_device(0)
	caffe.set_mode_gpu()
	solver = caffe.SGDSolver('./solver.prototxt')
	for i in range(260200):
		sys.stdout.flush()
		solver.step(1)
		#print solver.net.params['convF'][0].data[0].sum()
		#print 'iteration ' + str(i+1) + ' is done'




