#!/usr/bin/python
import os,math
import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import sys
import caffe
import cv2
import Image
matplotlib.rcParams['backend'] = "Qt4Agg"
import numpy as np
import lmdb
import skimage
#import skimage.measure
from sklearn import svm
caffe_root = '/home/,,/,,/opt/caffe'


MODEL_FILE = 'deploy.prototxt'
PRETRAINED = 'models/ckpt_iter_20000.caffemodel'
#PRETRAINED = 'load/tracker.caffemodel'

net = caffe.Net(MODEL_FILE, PRETRAINED,caffe.TEST)
caffe.set_device(0)
caffe.set_mode_gpu()
#for f in net.params['convF'][0].data:
#	print(f)
#	raw_input()
#for f in net.params['conv1'][0].data:
#	for f2 in f:
#		np.savetxt('/opt/caffe/brian_models/regression/l2.csv', f2, delimiter=',', fmt='%.3f')
#		print(f2)
#		raw_input()


lmdb_env = lmdb.open('imuScale')


lmdb_txn = lmdb_env.begin()
lmdb_cursor = lmdb_txn.cursor()
count = 0
correct = 0

p_label = []
t_y1 = []
ent = []



for key, value in lmdb_cursor:
	print "Count:"
	print count
	count = count + 1
	datum = caffe.proto.caffe_pb2.Datum()
	datum2= caffe.proto.caffe_pb2.Datum()
	datum.ParseFromString(value)
	#t_y1.append(int(datum.label))
	image = caffe.io.datum_to_array(datum)
	image = image.astype(np.float)
	im = image#[0,i1_start:i1_stop, i2_start:i2_stop]
	out = net.forward_all(imu=np.asarray([im]))
	#out = net.forward_all(data=np.asarray([im]), target=np.asarray([image2]))
	p_label.append(out['fc2'][0])
	#p_label.append(out['prob'][0].argmax(axis=0))
	#print("Label is class " + str(int(datum.label)) + ", predicted class is " + str(out['prob'][0].argmax(axis=0)))



lmdb_env = lmdb.open('labels')

lmdb_txn = lmdb_env.begin()
lmdb_cursor = lmdb_txn.cursor()
count = 0

for key, value in lmdb_cursor:
	#print "Count:"
	#print count
	count = count + 1
	datum = caffe.proto.caffe_pb2.Datum()
	datum.ParseFromString(value)
	truth = caffe.io.datum_to_array(datum)
	truth = truth.astype(np.float)
	t_y1.append(truth)
	#p_label.append(out['prob'][0].argmax(axis=0))
	#print("Label is class " + str(int(datum.label)) + ", predicted class is " + str(out['prob'][0].argmax(axis=0)))



np.savetxt('prediction.csv', p_label, delimiter=',', fmt='%.3f')
np.savetxt('truth.csv', t_y1, delimiter=',', fmt='%.3f')
#np.savetxt('wh.csv', [p_label[:][2]-p_label[:][0], p_label[:][3]-p_label[:][1]], delimiter=',', fmt='%.3f')


