#!/usr/bin/env python2
import cv2
import time
import os
import datetime
import shutil

TIME      = 15  # seconds
BASE_PATH = os.path.expanduser('~/')
camera    = None
cam_num   = 0
dirname   = 'TEST_SET' #'%m-%d-%Y_%H:%M:%S'

def get_images(secs, outdir):
    t_end = time.time() + secs
    cnt = 1
    imgs = []
    f_name = outdir + '/img{}.png'

    # grab 30 junk frames to let camera adjust
    for _ in range(30):
        _, im = camera.read()

    while time.time() < t_end:
        _, im = camera.read()
        imgs.append(im)
        cnt += 1

    for n, img in enumerate(imgs):
        cv2.imwrite(f_name.format(n), img)

    return cnt


if __name__ == '__main__':

    camera = cv2.VideoCapture(cam_num)

    # create directory in BASE_PATH with current datetime
    out_dir = os.path.join(
        BASE_PATH, datetime.datetime.now().strftime(dirname))

    if os.path.exists(out_dir):
        shutil.rmtree(out_dir)
    
    os.makedirs(out_dir)

    cnt = get_images(TIME, out_dir)  # grab frames for TIME seconds

    print('Grabbed {} frames at avg of {} fps'.format(cnt, cnt/TIME))

    del(camera)
