
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <std_msgs/Int32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/String.h>

#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Vector3.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/foreach.hpp>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>

std::string getLine(sensor_msgs::ImuConstPtr imu) 
{
	std::stringstream ss;

	ss << imu->orientation.x << "," << imu->orientation.y << "," << imu->orientation.z << "," << imu->orientation.w << ",";
	ss << imu->angular_velocity.x << "," << imu->angular_velocity.y << "," << imu->angular_velocity.z << ",";
	ss << imu->linear_acceleration.x << "," << imu->linear_acceleration.y << "," << imu->linear_acceleration.z << "\n";

	return ss.str();
}

void show_usage()
{
	std::cout << "Usage: bagParse [OPTION] SOURCE DEST\n"
		  << ">> Parse rosbag at SOURCE into output directory DEST."
		  << std::endl;
}

int main(int argc, char **argv)
{

	if (argc != 3) {
		// show usage and exit
		show_usage();
		return 0;
	}

	std::string sourcePath = argv[1];
	std::string destPath   = argv[2];

	// try to open rosbag
	rosbag::Bag bag;
	try {
		bag.open(sourcePath, rosbag::bagmode::Read);
	} catch(std::exception e) {
		std::cout << "ERROR> Could not open bag file at: \n\t"
			  << sourcePath << std::endl;
		return 0;
	}

	std::vector<std::string> topics;
	topics.push_back(std::string("/imu"));
	topics.push_back(std::string("/camera/color/image_raw"));

	rosbag::View view(bag, rosbag::TopicQuery(topics));

	sensor_msgs::ImuConstPtr imuCurr, imu;
	std::ostringstream csvOut;

	// add header to CSV
	csvOut << "img,orienX,orienY,orienZ,orienW,angVelX,angVelY,angVelZ,linAccelX,linAccelY,linAccelZ\n";

	int imgCnt = 0;

	BOOST_FOREACH(rosbag::MessageInstance const m, view)
	{
		// this message has imu data
		imuCurr = m.instantiate<sensor_msgs::Imu>();
		if (imuCurr != NULL) {
			imu = imuCurr;
		}

		// this message has image data and IMU data has been retrieved
		sensor_msgs::Image::ConstPtr img = m.instantiate<sensor_msgs::Image>();
		if (img != NULL && imu != NULL) {
			
			// get image and write to file
			cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);

			std::stringstream imgName;
			imgName << destPath << "/img" << imgCnt << ".png";
			try{
				cv::imwrite(imgName.str(), cv_ptr->image);
			} catch(std::exception e) {
				std::cout << "ERROR> could not write " << imgCnt << ".png" << std::endl;
			}
			
			// add imu data to output stream
			csvOut << "img" << imgCnt << "," << getLine(imu);

			// maintain image counter
			imgCnt++;
		}
	}

	bag.close();

	// write IMU data to imu.csv
	std::ofstream myfile;
	try {
		destPath += "/imu.csv";
		myfile.open(destPath.c_str());
		myfile << csvOut.str();
		myfile.close();
	} catch(std::exception std) {
		std::cout << "ERROR> could not open imu.csv" << std::endl;
	}
}
